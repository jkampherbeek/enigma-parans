program UtParans;

{$mode objfpc}{$H+}

uses
  Interfaces, Forms, GuiTestRunner, TestUnitAstron, UnitAstron, UnitAPI,
  unithandlers, UnitReferences, UnitDomainXchg, testunitfesupport,
  unitfesupport, unitformatting, testunitcentralcontroller,
  unitcentralcontroller, testunitdomainxchg, testunitformatting;

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TGuiTestRunner, TestRunner);
  Application.Run;
end.

