{ Jan Kampherbeek, (c)  2021.
  Enigma Parans is open source.
  Please check the file copyright.txt in the root of the source for further details }

unit itapi;

{$mode objfpc}{$H+}

interface

uses
  Classes, fpcunit, SysUtils, testregistry, UnitApi, UnitDomainXchg;

type

  { ParansIt }

  ParansIt = class(TTestCase)
  protected
    Api: TAllParansApi;
    procedure SetUp; override;
    procedure TearDown; override;
    function CreateRequest: TParansPosRequest;
  published
    procedure TestHappyFlow;
  end;

implementation

const
  MARGIN: double = 0.00000001;

procedure ParansIt.SetUp;
begin
  Api := TAllParansApi.Create;
  inherited SetUp;
end;

procedure ParansIt.TearDown;
begin
  FreeAndNil(Api);
  inherited TearDown;
end;

function ParansIt.CreateRequest: TParansPosRequest;
var
  Julian: double;
  Location: TLocation;
  HouseSystem: string;
  FixStarSet: TEnumFixStarSets;
  SolSysPointSet: TEnumSolSysPointSets;
  Settings: TSettings;
  ModernPlanets: boolean;
  ORbSeconds: integer;
begin
  ModernPlanets:= false;
  OrbSeconds:= 120;
  Settings := TSettings.Create(ModernPlanets, OrbSeconds);
  Location.Longitude := 6.9;
  Location.Latitude := 52.2166667;
  Julian := 2459474.1041666665;
  HouseSystem := 'W';
  FixStarSet := ptolemy;
  SolSysPointSet := classical;
  Result := TParansPosRequest.Create(FixStarSet, SolSysPointSet, Julian, Location, HouseSystem, Settings);
end;

procedure ParansIt.TestHappyFlow;
var
  Response: TParansPosResponse;
  Request: TParansPosRequest;
begin
  Request := CreateRequest;
  Response := Api.GetParans(Request);
  AssertEquals('', Response.ErrorTxt);
  AssertEquals(173.9437940887, Response.ParanCalculationResult.SolSysPoints[0].FullEclPos[0], MARGIN);
  AssertEquals(7, Length(Response.ParanCalculationResult.SolSysPoints));
  AssertEquals(29, Length(Response.ParanCalculationResult.FixStars));
  AssertEquals(222.603856961181, Response.ParanCalculationResult.HouseCusps.Mc, MARGIN);
  AssertEquals(275.8715, Response.ParanCalculationResult.HouseCusps.Asc);
  { TODO : Check values for unit tests }
  AssertEquals(2459473.7181189, Response.ParanCalculationResult.SolSysPoints[0].ParanTimes[0], MARGIN);
  AssertEquals(2459473.97720598, Response.ParanCalculationResult.SolSysPoints[0].ParanTimes[1], MARGIN);
  AssertEquals(2459474.23557498, Response.ParanCalculationResult.SolSysPoints[0].ParanTimes[2], MARGIN);
  AssertEquals(2459474.47708131, Response.ParanCalculationResult.SolSysPoints[0].ParanTimes[3], MARGIN);
end;



initialization

  RegisterTest(ParansIt);
end.

