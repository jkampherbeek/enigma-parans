{ Jan Kampherbeek, (c)  2021.
  Enigma Parans is open source.
  Please check the file copyright.txt in the root of the source for further details }

unit testunitdomainxchg;

{$mode objfpc}{$H+}

interface

uses
  Classes, fpcunit, SysUtils, testregistry, UnitDomainXchg;

type


  { TTestSettings }
  { Requires ini file with default settings. }
  TTestSettings = class(TTestCase)
  protected
    ModernPlanets: boolean;
    OrbSeconds: integer;
    Settings, Settings2: TSettings;
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestModernPlanets;
    procedure TestOrbSeconds;
    procedure TestOrbSecondsEmptyConstructor;
  end;

  { TTestInputValues }

  TTestInputValues = class(TTestCase)
  protected
    TimeZoneOffset: double;
    Dst, GregCal: boolean;
    InputValues: TInputValues;
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestTimeZone;
    procedure TestDst;
    procedure TestGregCal;
  end;

  { TTestFullDateTime }

  TTestFullDateTime = class(TTestCase)
  protected
    FullDateTime: TFullDateTime;
    Year, Month, Day, Hour, Minute, Second: integer;
    JulianDay, TzOffset: double;
    Dst, GregCal: boolean;
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestConstructorWithDateTimeHappyFlow;
    procedure TestConstructorWithDateTimePlusDst;
    procedure TestConstructorWithJdHappyFlow;
    procedure TestConstructorWithJdOffsetDst;
    procedure TestYear5Positions;
    procedure TestNegativeYear;
    procedure TestNegativeYear5Positions;
  end;

  { TTestFullParanPos }

  TTestFullParanPos = class(TTestCase)
  protected
    FullParanPos: TFullParanPos;
    CelPointSpec: TCelPointSpec;
    FullEclPos, EquatorialPos, HorizontalPos, ParanTimes: TDblArray;
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestEclipticalPositions;
    procedure TestEquatorialPositions;
    procedure TestHorizontalPositions;
    procedure TestParanTimes;
    procedure TestCelPointSpec;
  end;

  { TTestFixStarSpec }

  TTestFixStarSpec = class(TTestCase)
  protected
    Id: integer;
    Name, FormalName, SearchArg: string;
    FixStarSpec: TFixStarSpec;
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestId;
    procedure TestName;
    procedure TestFormalName;
    procedure TestSearchArg;
  end;

  { TTestSolSysPointSpec }

  TTestSolSysPointSpec = class(TTestCase)
  protected
    Id, SeId: integer;
    Name: string;
    SolSysPointSpec: TSolSysPointSpec;
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestId;
    procedure TestSeId;
    procedure TestName;
  end;

  { TTestTimezoneSpec }

  TTestTimezoneSpec = class(TTestCase)
  protected
    Name: string;
    Offset: double;
    TimeZoneSpec: TTimeZoneSpec;
     procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestName;
    procedure TestOffset;
  end;

  { TTestHouseCUsps }

  TTestHouseCusps = class(TTestCase)
  protected
    Mc, Asc, McDec, AscDecl: double;
    Cusps, CuspsDecl: TDblArray;
    HouseCusps: THouseCusps;
    procedure SetUp; override;
   procedure TearDown; override;
  published
    procedure TestMc;
    procedure TestAsc;
    procedure TestMcDecl;
    procedure TestAscDecl;
    procedure TestCusps;
    procedure TestCuspsDecl;
  end;


implementation

const
  MARGIN = 0.00000001;

{ TTestHouseCusps }

procedure TTestHouseCusps.SetUp;
var
  i: integer;
begin
  Mc := 100.0;
  Asc:= 200.0;
  McDec := 5.0;
  AscDecl:= -10.0;
  for i:= Low(Cusps) to High(Cusps) do Cusps[i]:= (i * 30) + 12.0;
  for i:= Low(CuspsDecl) to High(CuspsDecl) do CuspsDecl[i]:= i - 6.0;
  HouseCusps := THouseCusps.Create(Mc, Asc, McDec, AscDecl, Cusps, CuspsDecl);
  inherited SetUp;
end;

procedure TTestHouseCusps.TearDown;
begin
  FreeAndNil(HouseCusps);
  inherited TearDown;
end;

procedure TTestHouseCusps.TestMc;
begin
  AssertEquals(Mc, HouseCusps.Mc, MARGIN);
end;

procedure TTestHouseCusps.TestAsc;
begin
  AssertEquals(Asc, HouseCusps.Asc, MARGIN);
end;

procedure TTestHouseCusps.TestMcDecl;
begin
  AssertEquals(McDec, HouseCusps.McDecl, MARGIN);
end;

procedure TTestHouseCusps.TestAscDecl;
begin
  AssertEquals(AscDecl, HouseCusps.AscDecl, MARGIN);
end;

procedure TTestHouseCusps.TestCusps;
begin
  AssertEquals(Length(Cusps), Length(HouseCusps.Cusps));
end;

procedure TTestHouseCusps.TestCuspsDecl;
begin
  AssertEquals(Length(CuspsDecl), Length(HouseCusps.CuspsDecl));
end;

{ TTestTimezoneSpec }

procedure TTestTimezoneSpec.SetUp;
begin
  Name:= 'CET';
  Offset := 1.0;
  TimeZoneSpec:= TTimeZoneSpec.Create(Name, Offset);
  inherited SetUp;
end;

procedure TTestTimezoneSpec.TearDown;
begin
  FreeAndNil(TimeZoneSpec);
  inherited TearDown;
end;

procedure TTestTimezoneSpec.TestName;
begin
  AssertEquals(Name, TimeZoneSpec.Name);
end;

procedure TTestTimezoneSpec.TestOffset;
begin
  AssertEquals(Offset, TimeZoneSpec.Offset, MARGIN);
end;

{ TTestSolSysPointSpec }

procedure TTestSolSysPointSpec.SetUp;
begin
  Id := 33;
  SeId := 44;
  Name := 'SolSysPointName';
  SolSysPointSpec := TSolSysPointSpec.Create(Id, SeId, Name);
  inherited SetUp;
end;

procedure TTestSolSysPointSpec.TearDown;
begin
  FreeAndNil(SolSysPointSpec);
  inherited TearDown;
end;

procedure TTestSolSysPointSpec.TestId;
begin
  AssertEquals(Id, SolSysPointSpec.Id);
end;

procedure TTestSolSysPointSpec.TestSeId;
begin
  AssertEquals(SeId, SolSysPointSpec.SeId);
end;

procedure TTestSolSysPointSpec.TestName;
begin
  AssertEquals(Name, SolSysPointSpec.Name);
end;

{ TTestFixStarSpec }

procedure TTestFixStarSpec.SetUp;
begin
  Id := 123;
  Name := 'FullName';
  FormalName := 'FormalName';
  SearchArg := 'SearchArg';
  FixStarSpec := TFixStarSpec.Create(Id, Name, FormalName, SearchArg);
  inherited SetUp;
end;

procedure TTestFixStarSpec.TearDown;
begin
  FreeAndNil(FixStarSpec);
  inherited TearDown;
end;

procedure TTestFixStarSpec.TestId;
begin
  AssertEquals(Id, FixStarSpec.Id);
end;

procedure TTestFixStarSpec.TestName;
begin
  AssertEquals(Name, FixStarSpec.Name);
end;

procedure TTestFixStarSpec.TestFormalName;
begin
  AssertEquals(FormalName, FixStarSpec.FormalName);
end;

procedure TTestFixStarSpec.TestSearchArg;
begin
  AssertEquals(SearchArg, FixStarSpec.SearchArg);
end;

{ TTestInputValues }

procedure TTestInputValues.SetUp;
begin
  TimeZoneOffset := 2.0;
  Dst := False;
  GregCal := True;
  InputValues := TInputValues.Create(TimeZoneOffset, Dst, GregCal);
  inherited SetUp;
end;

procedure TTestInputValues.TearDown;
begin
  FreeAndNil(InputValues);
  inherited TearDown;
end;

procedure TTestInputValues.TestTimeZone;
begin
  AssertEquals(TimeZoneOffset, InputValues.TimezoneOffset, MARGIN);
end;

procedure TTestInputValues.TestDst;
begin
  AssertEquals(Dst, InputValues.Dst);
end;

procedure TTestInputValues.TestGregCal;
begin
  AssertEquals(GregCal, InputValues.GregCal);
end;

{ TTestSettings }

procedure TTestSettings.SetUp;
begin
  Settings2 := TSettings.Create;
  ModernPlanets := False;
  OrbSeconds := 123;
  Settings := TSettings.Create(ModernPlanets, OrbSeconds);
  inherited SetUp;
end;

procedure TTestSettings.TearDown;
begin
  FreeAndNil(Settings);
  inherited TearDown;
end;

procedure TTestSettings.TestModernPlanets;
begin
  AssertEquals(ModernPlanets, Settings.ModernPlanets);
end;

procedure TTestSettings.TestOrbSeconds;
begin
  AssertEquals(OrbSeconds, Settings.OrbSeconds);
end;

procedure TTestSettings.TestOrbSecondsEmptyConstructor;
begin
  AssertEquals(540, Settings2.OrbSeconds);
end;


{ TTestFullParanPos }

procedure TTestFullParanPos.SetUp;
begin
  CelPointSpec := TCelPointSpec.Create(0, 'Sun');
  SetLength(FullEclPos, 6);
  SetLength(EquatorialPos, 2);
  SetLength(HorizontalPos, 2);
  SetLength(ParanTimes, 4);
  FullEclPos[0] := 0.1;
  FullEclPos[1] := 0.2;
  FullEclPos[2] := 0.3;
  FullEclPos[3] := 0.4;
  FullEclPos[4] := 0.5;
  FullEclPos[5] := 0.6;
  EquatorialPos[0] := 1.1;
  EquatorialPos[1] := 1.2;
  HorizontalPos[0] := 2.1;
  HorizontalPos[1] := 2.2;
  ParanTimes[0] := 3.1;
  ParanTimes[1] := 3.2;
  ParanTimes[2] := 3.3;
  ParanTimes[3] := 3.4;
  FullParanPos := TFullParanPos.Create(CelPointSpec, FullEclPos, EquatorialPos, HorizontalPos, ParanTimes);
  inherited SetUp;
end;

procedure TTestFullParanPos.TearDown;
begin
  FreeAndNil(CelPointSpec);
  inherited TearDown;
end;

procedure TTestFullParanPos.TestEclipticalPositions;
begin
  AssertEquals(0.1, FullParanPos.FullEclPos[0], MARGIN);
  AssertEquals(0.2, FullParanPos.FullEclPos[1], MARGIN);
  AssertEquals(0.3, FullParanPos.FullEclPos[2], MARGIN);
  AssertEquals(0.4, FullParanPos.FullEclPos[3], MARGIN);
  AssertEquals(0.5, FullParanPos.FullEclPos[4], MARGIN);
  AssertEquals(0.6, FullParanPos.FullEclPos[5], MARGIN);
end;

procedure TTestFullParanPos.TestEquatorialPositions;
begin
  AssertEquals(1.1, EquatorialPos[0], MARGIN);
  AssertEquals(1.2, EquatorialPos[1], MARGIN);
end;

procedure TTestFullParanPos.TestHorizontalPositions;
begin
  AssertEquals(2.1, FullParanPos.HorizontalPos[0], MARGIN);
  AssertEquals(2.2, FullParanPos.HorizontalPos[1], MARGIN);
end;

procedure TTestFullParanPos.TestParanTimes;
begin
  AssertEquals(3.1, FullParanPos.ParanTimes[0], MARGIN);
  AssertEquals(3.2, FullParanPos.ParanTimes[1], MARGIN);
  AssertEquals(3.3, FullParanPos.ParanTimes[2], MARGIN);
  AssertEquals(3.4, FullParanPos.ParanTimes[3], MARGIN);
end;

procedure TTestFullParanPos.TestCelPointSpec;
begin
  AssertEquals(0, FullParanPos.CelPointSpec.Id);
  AssertEquals('Sun', FullParanPos.CelPointSpec.Name);
end;

{ TTestFullDatetime }

procedure TTestFullDateTime.TestConstructorWithDateTimeHappyFlow;
begin
  FullDateTime := TFullDatetime.Create(Year, Month, Day, Hour, Minute, Second, TzOffset, Dst, GregCal);
  AssertEquals(1953, FullDateTime.Year);
  AssertEquals(8.625, FullDateTime.ClockTime, MARGIN);
  AssertEquals(' 1953/01/29 08:37:30', FullDateTime.FormattedText);
end;

procedure TTestFullDateTime.TestConstructorWithDateTimePlusDst;
var
  ChangedDst: boolean;
begin
  ChangedDst := True;
  FullDateTime := TFullDatetime.Create(Year, Month, Day, Hour, Minute, Second, TzOffset, ChangedDst, GregCal);
  AssertEquals(8.625, FullDateTime.ClockTime, MARGIN);
  AssertEquals(8, FullDateTime.Hour);
  AssertEquals(' 1953/01/29 08:37:30', FullDateTime.FormattedText);
end;

procedure TTestFullDateTime.TestConstructorWithJdHappyFlow;
var
  ChangedTzOffset: integer;
begin
  ChangedTzOffset := 0;
  FullDateTime := TFullDatetime.Create(JulianDay, ChangedTzOffset, Dst, GregCal);
  AssertEquals(7.625, FullDateTime.ClockTime, MARGIN);
  AssertEquals(7, FullDateTime.Hour);
  AssertEquals(' 1953/01/29 07:37:30', FullDateTime.FormattedText);
end;

procedure TTestFullDateTime.TestConstructorWithJdOffsetDst;
var
  ChangedTzOffset: integer;
  ChangedDst: boolean;
begin
  ChangedTzOffset := 1;
  ChangedDst := True;
  FullDateTime := TFullDatetime.Create(JulianDay, ChangedTzOffset, ChangedDst, GregCal);
  AssertEquals(9.625, FullDateTime.ClockTime, MARGIN);
  AssertEquals(9, FullDateTime.Hour);
  AssertEquals(' 1953/01/29 09:37:30', FullDateTime.FormattedText);

end;

procedure TTestFullDateTime.TestYear5Positions;
var
  ChangedYear: integer;
begin
  ChangedYear := 12000;
  FullDateTime := TFullDatetime.Create(ChangedYear, Month, Day, Hour, Minute, Second, TzOffset, Dst, GregCal);
  AssertEquals(12000, FullDateTime.Year);
  AssertEquals(8.625, FullDateTime.ClockTime, MARGIN);
  AssertEquals('12000/01/29 08:37:30', FullDateTime.FormattedText);
end;

procedure TTestFullDateTime.TestNegativeYear;
var
  ChangedYear: integer;
begin
  ChangedYear := -1100;
  FullDateTime := TFullDatetime.Create(ChangedYear, Month, Day, Hour, Minute, Second, TzOffset, Dst, GregCal);
  AssertEquals(-1100, FullDateTime.Year);
  AssertEquals(8.625, FullDateTime.ClockTime, MARGIN);
  AssertEquals('-1100/01/29 08:37:30', FullDateTime.FormattedText);
end;

procedure TTestFullDateTime.TestNegativeYear5Positions;
var
  ChangedYear: integer;
begin
  ChangedYear := -11000;
  FullDateTime := TFullDatetime.Create(ChangedYear, Month, Day, Hour, Minute, Second, TzOffset, Dst, GregCal);
  AssertEquals(-11000, FullDateTime.Year);
  AssertEquals(8.625, FullDateTime.ClockTime, MARGIN);
  AssertEquals('-11000/01/29 08:37:30', FullDateTime.FormattedText);
end;


procedure TTestFullDateTime.SetUp;
begin
  Year := 1953;
  Month := 1;
  Day := 29;
  Hour := 8;
  Minute := 37;
  Second := 30;
  TzOffset := 1.0;
  Dst := False;
  GregCal := True;
  JulianDay := 2434406.817708333333;
end;

procedure TTestFullDateTime.TearDown;
begin
  FreeAndNil(FullDatetime);
end;

initialization
  RegisterTest(TTestSettings);
  RegisterTest(TTestInputValues);
  RegisterTest(TTestFullDateTime);
  RegisterTest(TTestFullParanPos);
  RegisterTest(TTestFixStarSpec);
  RegisterTest(TTestSolSysPointSpec);
  RegisterTest(TTestTimezoneSpec);
  RegisterTest(TTestHouseCUsps);
end.

