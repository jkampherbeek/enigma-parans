{ Jan Kampherbeek, (c)  2021.
  Enigma Parans is open source.
  Please check the file copyright.txt in the root of the source for further details }

unit testunitfesupport;

{$mode objfpc}{$H+}

interface

uses
  Classes, fpcunit, SysUtils, testregistry, UnitDomainXchg, unitfesupport;

type

  { TTestSexagesimalConverter }
  TTestSexagesimalConverter = class(TTestCase)
  protected
    Converter: TSexagesimalConverter;
    DegOrH, Minute, Second, MinDegH, MaxDegH: integer;
    ValDouble: TValidatedDouble;
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure ThreeInts2DecimalHappyFlow;
    procedure ThreeInts2DecimalDegOrHTooLarge;
    procedure ThreeInts2DecimalDegOrHTooSmall;
    procedure ThreeInts2DecimalMinuteTooLarge;
    procedure ThreeInts2DecimalMinuteTooSmall;
    procedure ThreeInts2DecimalSecondTooLarge;
    procedure ThreeInts2DecimalSecondTooSmall;
    procedure ThreeInts2DecimalNegative;
  end;


  { TTestTextConverter }

  TTestTextConverter = class(TTestCase)
  protected
    Converter: TTextConverter;
    Texts: array of string;
    IntResult: TConvertedInts;
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure ThreeTexts2ThreeIntsHappyFlow;
    procedure ThreeTexts2ThreeIntsWrongLength;
    procedure ThreeTexts2ThreeIntsNonNumeric;
    procedure ThreeTexts2ThreeIntsUsingDoubles;
    procedure ThreeTexts2ThreeIntsUsingNegatives;
    procedure ThreeTexts2ThreeIntsUsingZeros;
  end;

  { TTestChartDataValidator }

  TTestChartDataValidator = class(TTestCase)
  protected
    ChartDataValidator: TChartDataValidator;
    ValidatedDouble: TValidatedDouble;
    ValidatedInteger: TValidatedInteger;
    Meters, DegOrH, Minute, Second, Day, Month, Year: string;
    Dir: TGeoDirSpec;
    YearCount: TYearCountSpec;
    Calendar: TCalendarSpec;
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure ValidatorLongitudeHappyFlow;
    procedure ValidatorLongitudeNonNumeric;
    procedure ValidatorLongitudeDegreeOutOfRange;
    procedure ValidatorLongitudeMinuteOutOfRange;
    procedure ValidatorLongitudeNegative;
    procedure ValidatorLatitudeHappyFlow;
    procedure ValidatorLatitudeNonNumeric;
    procedure ValidatorLatitudeDegreeOutOfRange;
    procedure ValidatorLatitudeSecondOutOfRange;
    procedure ValidatorLatitudeNegative;
    procedure ValidatorAltitudeHappyFlow;
    procedure ValidatorAltitudeNegative;
    procedure ValidatorAltitudeEmpty;
    procedure ValidatorAltitudeNonNumeric;
    procedure ValidatorAltitudeFloat;
    procedure ValidatorDateHappyFlow;
    procedure ValidatorDateNonNumeric;
    procedure ValidatorDateMonthOutOfRange;
    procedure ValidatorDateDayOutOfRange;
    procedure ValidatorDateYearCEOutOfRange;
    procedure ValidatorDateYearBCEOutOfRange;
    procedure ValidatorDateYearAstronNegativeOutOfRange;
    procedure ValidatorDateLeapDayGregorian;
    procedure ValidatorDateLeapDayJulian;
    procedure ValidatorDateLeapDayBCE;
    procedure ValidatorDateLeapDayAstronNegative;
    procedure ValidatorTimeHappyFlow;
    procedure ValidatorTimeNonNumeric;
    procedure ValidatorTimeHourOutOfRange;
    procedure ValidatorTimeMinuteOutOfRange;
    procedure ValidatorTimeSecondOutOfRange;
  end;

  { TTestSettingsValidator }

  TTestSettingsValidator = class(TTestCase)
  protected
    Validator: TSettingsValidator;
    valInt: TValidatedInteger;
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestHandleMinutesHappyFlow;
    procedure TestHandleMinutesNotNumeric;
    procedure TestHandleMinutesTooLarge;
    procedure TestHandleMinutesTooSmall;
    procedure TestHandleMinutesEmptyString;
    procedure TestHandleSecondsHappyFlow;
    procedure TestHandleSecondsNotNumeric;
    procedure TestHandleSecondsTooLarge;
    procedure TestHandleSecondsTooSmall;
    procedure TestHandleSecondsEmptyString;
  end;

implementation

const
  MARGIN = 0.00000001;

{ TTestSettingsValidator }

procedure TTestSettingsValidator.SetUp;
begin
  Validator := TSettingsValidator.Create;
  inherited SetUp;
end;

procedure TTestSettingsValidator.TearDown;
begin
  FreeAndNil(Validator);
  inherited TearDown;
end;

procedure TTestSettingsValidator.TestHandleMinutesHappyFlow;
begin
  valInt := Validator.HandleMinutes('5');
  assertTrue(valInt.Valid);
  assertEquals(5, valInt.Value);
end;

procedure TTestSettingsValidator.TestHandleMinutesNotNumeric;
begin
  valInt := Validator.HandleMinutes('s');
  assertFalse(valInt.Valid);
end;

procedure TTestSettingsValidator.TestHandleMinutesTooLarge;
begin
  valInt := Validator.HandleMinutes('100');
  assertFalse(valInt.Valid);
end;

procedure TTestSettingsValidator.TestHandleMinutesTooSmall;
begin
  valInt := Validator.HandleMinutes('-2');
  assertFalse(valInt.Valid);
end;

procedure TTestSettingsValidator.TestHandleMinutesEmptyString;
begin
  valInt := Validator.HandleMinutes('');
  assertFalse(valInt.Valid);
end;

procedure TTestSettingsValidator.TestHandleSecondsHappyFlow;
begin
  valInt := Validator.HandleSeconds('12');
  assertTrue(valInt.Valid);
  assertEquals(12, valInt.Value);
end;

procedure TTestSettingsValidator.TestHandleSecondsNotNumeric;
begin
  valInt := Validator.HandleSeconds('2x');
  assertFalse(valint.Valid);
end;

procedure TTestSettingsValidator.TestHandleSecondsTooLarge;
begin
  valInt := Validator.HandleSeconds('60');
  assertFalse(valint.Valid);
end;

procedure TTestSettingsValidator.TestHandleSecondsTooSmall;
begin
  valInt := Validator.HandleSeconds('-3');
  assertFalse(valint.Valid);
end;

procedure TTestSettingsValidator.TestHandleSecondsEmptyString;
begin
  valInt := Validator.HandleSeconds('');
  assertFalse(valint.Valid);
end;


{ TTestChartDataValidator }

procedure TTestChartDataValidator.SetUp;
begin
  ChartDataValidator := TChartDataValidator.Create;
  inherited SetUp;
end;

procedure TTestChartDataValidator.TearDown;
begin
  FreeAndNil(ChartDataValidator);
  inherited TearDown;
end;

procedure TTestChartDataValidator.ValidatorLongitudeHappyFlow;
begin
  DegOrH := '6';
  Minute := '54';
  Second := '0';
  Dir.Name := 'not relevant';
  Dir.PlusMinus := 1;
  ValidatedDouble := ChartDataValidator.HandleLongitude(DegOrH, Minute, Second, Dir);
  AssertTrue(ValidatedDouble.Valid);
  AssertEquals(6.9, ValidatedDouble.Value, MARGIN);
end;

procedure TTestChartDataValidator.ValidatorLongitudeNonNumeric;
begin
  DegOrH := '6';
  Minute := 'x';
  Second := '0';
  Dir.Name := 'not relevant';
  Dir.PlusMinus := 1;
  ValidatedDouble := ChartDataValidator.HandleLongitude(DegOrH, Minute, Second, Dir);
  AssertFalse(ValidatedDouble.Valid);
end;

procedure TTestChartDataValidator.ValidatorLongitudeDegreeOutOfRange;
begin
  DegOrH := '181';
  Minute := '23';
  Second := '15';
  Dir.Name := 'not relevant';
  Dir.PlusMinus := 1;
  ValidatedDouble := ChartDataValidator.HandleLongitude(DegOrH, Minute, Second, Dir);
  AssertFalse(ValidatedDouble.Valid);
end;

procedure TTestChartDataValidator.ValidatorLongitudeMinuteOutOfRange;
begin
  DegOrH := '6';
  Minute := '60';
  Second := '0';
  Dir.Name := 'not relevant';
  Dir.PlusMinus := 1;
  ValidatedDouble := ChartDataValidator.HandleLongitude(DegOrH, Minute, Second, Dir);
  AssertFalse(ValidatedDouble.Valid);
end;

procedure TTestChartDataValidator.ValidatorLongitudeNegative;
begin
  DegOrH := '6';
  Minute := '30';
  Second := '0';
  Dir.Name := 'not relevant';
  Dir.PlusMinus := -1;
  ValidatedDouble := ChartDataValidator.HandleLongitude(DegOrH, Minute, Second, Dir);
  AssertTrue(ValidatedDouble.Valid);
  AssertEquals(-6.5, ValidatedDouble.Value, MARGIN);
end;

procedure TTestChartDataValidator.ValidatorLatitudeHappyFlow;
begin
  DegOrH := '52';
  Minute := '13';
  Second := '0';
  Dir.Name := 'not relevant';
  Dir.PlusMinus := 1;
  ValidatedDouble := ChartDataValidator.HandleLatitude(DegOrH, Minute, Second, Dir);
  AssertTrue(ValidatedDouble.Valid);
  AssertEquals(52.216666666667, ValidatedDouble.Value, MARGIN);
end;

procedure TTestChartDataValidator.ValidatorLatitudeNonNumeric;
begin
  DegOrH := 'ab';
  Minute := '13';
  Second := '0';
  Dir.Name := 'not relevant';
  Dir.PlusMinus := 1;
  ValidatedDouble := ChartDataValidator.HandleLatitude(DegOrH, Minute, Second, Dir);
  AssertFalse(ValidatedDouble.Valid);
end;

procedure TTestChartDataValidator.ValidatorLatitudeDegreeOutOfRange;
begin
  DegOrH := '90';
  Minute := '0';
  Second := '0';
  Dir.Name := 'not relevant';
  Dir.PlusMinus := 1;
  ValidatedDouble := ChartDataValidator.HandleLatitude(DegOrH, Minute, Second, Dir);
  AssertFalse(ValidatedDouble.Valid);
end;

procedure TTestChartDataValidator.ValidatorLatitudeSecondOutOfRange;
begin
  DegOrH := '9';
  Minute := '0';
  Second := '60';
  Dir.Name := 'not relevant';
  Dir.PlusMinus := 1;
  ValidatedDouble := ChartDataValidator.HandleLatitude(DegOrH, Minute, Second, Dir);
  AssertFalse(ValidatedDouble.Valid);
end;

procedure TTestChartDataValidator.ValidatorLatitudeNegative;
begin
  DegOrH := '52';
  Minute := '13';
  Second := '0';
  Dir.Name := 'not relevant';
  Dir.PlusMinus := -1;
  ValidatedDouble := ChartDataValidator.HandleLatitude(DegOrH, Minute, Second, Dir);
  AssertTrue(ValidatedDouble.Valid);
  AssertEquals(-52.216666666667, ValidatedDouble.Value, MARGIN);
end;

procedure TTestChartDataValidator.ValidatorAltitudeHappyFlow;
begin
  Meters := '123';
  ValidatedInteger := ChartDataValidator.HandleAltitude(Meters);
  AssertTrue(ValidatedInteger.Valid);
  AssertEquals(123, ValidatedInteger.Value);
end;

procedure TTestChartDataValidator.ValidatorAltitudeNegative;
begin
  Meters := '-22';
  ValidatedInteger := ChartDataValidator.HandleAltitude(Meters);
  AssertTrue(ValidatedInteger.Valid);
  AssertEquals(-22, ValidatedInteger.Value);
end;

procedure TTestChartDataValidator.ValidatorAltitudeEmpty;
begin
  Meters := '';
  ValidatedInteger := ChartDataValidator.HandleAltitude(Meters);
  AssertTrue(ValidatedInteger.Valid);
  AssertEquals(0, ValidatedInteger.Value);
end;

procedure TTestChartDataValidator.ValidatorAltitudeNonNumeric;
begin
  Meters := 'abc';
  ValidatedInteger := ChartDataValidator.HandleAltitude(Meters);
  AssertFalse(ValidatedInteger.Valid);
end;

procedure TTestChartDataValidator.ValidatorAltitudeFloat;
begin
  Meters := '123.456';
  ValidatedInteger := ChartDataValidator.HandleAltitude(Meters);
  AssertFalse(ValidatedInteger.Valid);
end;

procedure TTestChartDataValidator.ValidatorDateHappyFlow;
begin
  Day := '29';
  Month := '1';
  Year := '1953';
  Calendar.Index := 0;            // Gregorian
  Calendar.Name := 'not relevant';
  YearCount.Index := 0;           // CE
  YearCount.Name := 'not relevant';
  ValidatedDouble := ChartDataValidator.HandleDate(Year, MOnth, Day, Calendar, YearCount);
  AssertTrue(ValidatedDouble.Valid);
  AssertEquals(2434406.5, ValidatedDouble.Value, MARGIN);
end;

procedure TTestChartDataValidator.ValidatorDateNonNumeric;
begin
  Day := '29';
  Month := 's';
  Year := '1953';
  Calendar.Index := 0;            // Gregorian
  Calendar.Name := 'not relevant';
  YearCount.Index := 0;           // CE
  YearCount.Name := 'not relevant';
  ValidatedDouble := ChartDataValidator.HandleDate(Year, MOnth, Day, Calendar, YearCount);
  AssertFalse(ValidatedDouble.Valid);
end;

procedure TTestChartDataValidator.ValidatorDateMonthOutOfRange;
begin
  Day := '29';
  Month := '13';
  Year := '1953';
  Calendar.Index := 0;            // Gregorian
  Calendar.Name := 'not relevant';
  YearCount.Index := 0;           // CE
  YearCount.Name := 'not relevant';
  ValidatedDouble := ChartDataValidator.HandleDate(Year, MOnth, Day, Calendar, YearCount);
  AssertFalse(ValidatedDouble.Valid);
end;

procedure TTestChartDataValidator.ValidatorDateDayOutOfRange;
begin
  Day := '-2';
  Month := '1';
  Year := '1953';
  Calendar.Index := 0;            // Gregorian
  Calendar.Name := 'not relevant';
  YearCount.Index := 0;           // CE
  YearCount.Name := 'not relevant';
  ValidatedDouble := ChartDataValidator.HandleDate(Year, MOnth, Day, Calendar, YearCount);
  AssertFalse(ValidatedDouble.Valid);
end;

procedure TTestChartDataValidator.ValidatorDateYearCEOutOfRange;
begin
  Day := '29';
  Month := '1';
  Year := '16799';
  Calendar.Index := 0;            // Gregorian
  Calendar.Name := 'not relevant';
  YearCount.Index := 0;           // CE
  YearCount.Name := 'not relevant';
  ValidatedDouble := ChartDataValidator.HandleDate(Year, MOnth, Day, Calendar, YearCount);
  AssertTrue(ValidatedDouble.Valid);
  Year := '17000';
  ValidatedDouble := ChartDataValidator.HandleDate(Year, MOnth, Day, Calendar, YearCount);
  AssertFalse(ValidatedDouble.Valid);
end;

procedure TTestChartDataValidator.ValidatorDateYearBCEOutOfRange;
begin
  Day := '29';
  Month := '1';
  Year := '12999';
  Calendar.Index := 1;            // Julian
  Calendar.Name := 'not relevant';
  YearCount.Index := 1;           // BCE
  YearCount.Name := 'not relevant';
  ValidatedDouble := ChartDataValidator.HandleDate(Year, Month, Day, Calendar, YearCount);
  AssertTrue(ValidatedDouble.Valid);
  Year := '13000';
  ValidatedDouble := ChartDataValidator.HandleDate(Year, Month, Day, Calendar, YearCount);
  AssertFalse(ValidatedDouble.Valid);
end;

procedure TTestChartDataValidator.ValidatorDateYearAstronNegativeOutOfRange;
begin
  Day := '2';
  Month := '8';
  Year := '-12999';
  Calendar.Index := 1;            // Julian
  Calendar.Name := 'not relevant';
  YearCount.Index := 2;           // Astronomical
  YearCount.Name := 'not relevant';
  ValidatedDouble := ChartDataValidator.HandleDate(Year, Month, Day, Calendar, YearCount);
  AssertTrue(ValidatedDouble.Valid);
  Year := '-13000';
  ValidatedDouble := ChartDataValidator.HandleDate(Year, Month, Day, Calendar, YearCount);
  AssertFalse(ValidatedDouble.Valid);
end;

procedure TTestChartDataValidator.ValidatorDateLeapDayGregorian;
begin
  Day := '29';
  Month := '2';
  Year := '1953';
  Calendar.Index := 0;            // Gregorian
  Calendar.Name := 'not relevant';
  YearCount.Index := 0;           // CE
  YearCount.Name := 'not relevant';
  ValidatedDouble := ChartDataValidator.HandleDate(Year, MOnth, Day, Calendar, YearCount);
  AssertFalse(ValidatedDouble.Valid);
  Year := '1952';
  ValidatedDouble := ChartDataValidator.HandleDate(Year, MOnth, Day, Calendar, YearCount);
  AssertTrue(ValidatedDouble.Valid);
  Year := '1900';
  ValidatedDouble := ChartDataValidator.HandleDate(Year, MOnth, Day, Calendar, YearCount);
  AssertFalse(ValidatedDouble.Valid);
  Year := '2000';
  ValidatedDouble := ChartDataValidator.HandleDate(Year, MOnth, Day, Calendar, YearCount);
  AssertTrue(ValidatedDouble.Valid);
end;

procedure TTestChartDataValidator.ValidatorDateLeapDayJulian;
begin
  Day := '29';
  Month := '2';
  Year := '1953';
  Calendar.Index := 1;            // Julian
  Calendar.Name := 'not relevant';
  YearCount.Index := 0;           // CE
  YearCount.Name := 'not relevant';
  ValidatedDouble := ChartDataValidator.HandleDate(Year, MOnth, Day, Calendar, YearCount);
  AssertFalse(ValidatedDouble.Valid);
  Year := '1952';
  ValidatedDouble := ChartDataValidator.HandleDate(Year, MOnth, Day, Calendar, YearCount);
  AssertTrue(ValidatedDouble.Valid);
  Year := '1900';
  ValidatedDouble := ChartDataValidator.HandleDate(Year, MOnth, Day, Calendar, YearCount);
  AssertTrue(ValidatedDouble.Valid);
  Year := '2000';
  ValidatedDouble := ChartDataValidator.HandleDate(Year, MOnth, Day, Calendar, YearCount);
  AssertTrue(ValidatedDouble.Valid);
end;

procedure TTestChartDataValidator.ValidatorDateLeapDayBCE;
begin
  Day := '29';
  Month := '2';
  Year := '97';
  Calendar.Index := 1;            // Julian
  Calendar.Name := 'not relevant';
  YearCount.Index := 1;           // BCE
  YearCount.Name := 'not relevant';
  ValidatedDouble := ChartDataValidator.HandleDate(Year, Month, Day, Calendar, YearCount);
  AssertTrue(ValidatedDouble.Valid);
  Year := '96';
  ValidatedDouble := ChartDataValidator.HandleDate(Year, Month, Day, Calendar, YearCount);
  AssertFalse(ValidatedDouble.Valid);
  Year := '10097';
  ValidatedDouble := ChartDataValidator.HandleDate(Year, Month, Day, Calendar, YearCount);
  AssertTrue(ValidatedDouble.Valid);
end;

procedure TTestChartDataValidator.ValidatorDateLeapDayAstronNegative;
begin
  Day := '29';
  Month := '2';
  Year := '-96';
  Calendar.Index := 1;            // Julian
  Calendar.Name := 'not relevant';
  YearCount.Index := 2;           // Astronomical
  YearCount.Name := 'not relevant';
  ValidatedDouble := ChartDataValidator.HandleDate(Year, MOnth, Day, Calendar, YearCount);
  AssertTrue(ValidatedDouble.Valid);
  Year := '-97';
  ValidatedDouble := ChartDataValidator.HandleDate(Year, MOnth, Day, Calendar, YearCount);
  AssertFalse(ValidatedDouble.Valid);
end;

procedure TTestChartDataValidator.ValidatorTimeHappyFlow;
begin
  DegOrH := '7';
  Minute := '37';
  Second := '30';
  ValidatedDouble := ChartDataValidator.HandleTime(DegOrH, Minute, Second);
  AssertTrue(ValidatedDouble.Valid);
  AssertEquals(7.625, ValidatedDouble.Value, MARGIN);
end;

procedure TTestChartDataValidator.ValidatorTimeNonNumeric;
begin
  DegOrH := '7';
  Minute := 'w7';
  Second := '30';
  ValidatedDouble := ChartDataValidator.HandleTime(DegOrH, Minute, Second);
  AssertFalse(ValidatedDouble.Valid);
end;

procedure TTestChartDataValidator.ValidatorTimeHourOutOfRange;
begin
  DegOrH := '27';
  Minute := '37';
  Second := '30';
  ValidatedDouble := ChartDataValidator.HandleTime(DegOrH, Minute, Second);
  AssertFalse(ValidatedDouble.Valid);
end;

procedure TTestChartDataValidator.ValidatorTimeMinuteOutOfRange;
begin
  DegOrH := '27';
  Minute := '-37';
  Second := '30';
  ValidatedDouble := ChartDataValidator.HandleTime(DegOrH, Minute, Second);
  AssertFalse(ValidatedDouble.Valid);
end;

procedure TTestChartDataValidator.ValidatorTimeSecondOutOfRange;
begin
  DegOrH := '27';
  Minute := '37';
  Second := '60';
  ValidatedDouble := ChartDataValidator.HandleTime(DegOrH, Minute, Second);
  AssertFalse(ValidatedDouble.Valid);
end;

{ TTestSexagesimalConverter }

procedure TTestSexagesimalConverter.SetUp;
begin
  Converter := TSexagesimalConverter.Create;
  inherited SetUp;
end;

procedure TTestSexagesimalConverter.TearDown;
begin
  FreeAndNil(Converter);
  inherited TearDown;
end;

procedure TTestSexagesimalConverter.ThreeInts2DecimalHappyFlow;
begin
  DegOrH := 12;
  Minute := 30;
  Second := 18;
  MinDegH := 0;
  MaxDegH := 59;
  ValDouble := Converter.ThreeInts2Decimal(DegOrH, Minute, Second, MinDegH, MaxDegH);
  AssertTrue(ValDouble.Valid);
  AssertEquals(12.505, ValDouble.Value, MARGIN);
end;


procedure TTestSexagesimalConverter.ThreeInts2DecimalDegOrHTooLarge;
begin
  DegOrH := 102;
  Minute := 30;
  Second := 18;
  MinDegH := 45;
  MaxDegH := 90;
  ValDouble := Converter.ThreeInts2Decimal(DegOrH, Minute, Second, MinDegH, MaxDegH);
  AssertFalse(ValDouble.Valid);
end;

procedure TTestSexagesimalConverter.ThreeInts2DecimalDegOrHTooSmall;
begin
  DegOrH := 22;
  Minute := 30;
  Second := 18;
  MinDegH := 45;
  MaxDegH := 90;
  ValDouble := Converter.ThreeInts2Decimal(DegOrH, Minute, Second, MinDegH, MaxDegH);
  AssertFalse(ValDouble.Valid);
end;

procedure TTestSexagesimalConverter.ThreeInts2DecimalMinuteTooLarge;
begin
  DegOrH := 12;
  Minute := 60;
  Second := 18;
  MinDegH := 0;
  MaxDegH := 59;
  ValDouble := Converter.ThreeInts2Decimal(DegOrH, Minute, Second, MinDegH, MaxDegH);
  AssertFalse(ValDouble.Valid);
end;

procedure TTestSexagesimalConverter.ThreeInts2DecimalMinuteTooSmall;
begin
  DegOrH := 12;
  Minute := -1;
  Second := 18;
  MinDegH := 0;
  MaxDegH := 59;
  ValDouble := Converter.ThreeInts2Decimal(DegOrH, Minute, Second, MinDegH, MaxDegH);
  AssertFalse(ValDouble.Valid);
end;

procedure TTestSexagesimalConverter.ThreeInts2DecimalSecondTooLarge;
begin
  DegOrH := 12;
  Minute := 1;
  Second := 78;
  MinDegH := 0;
  MaxDegH := 59;
  ValDouble := Converter.ThreeInts2Decimal(DegOrH, Minute, Second, MinDegH, MaxDegH);
  AssertFalse(ValDouble.Valid);
end;

procedure TTestSexagesimalConverter.ThreeInts2DecimalSecondTooSmall;
begin
  DegOrH := 12;
  Minute := 1;
  Second := -8;
  MinDegH := 0;
  MaxDegH := 59;
  ValDouble := Converter.ThreeInts2Decimal(DegOrH, Minute, Second, MinDegH, MaxDegH);
  AssertFalse(ValDouble.Valid);
end;

procedure TTestSexagesimalConverter.ThreeInts2DecimalNegative;
begin
  DegOrH := -12;
  Minute := 30;
  Second := 18;
  MinDegH := -90;
  MaxDegH := 90;
  ValDouble := Converter.ThreeInts2Decimal(DegOrH, Minute, Second, MinDegH, MaxDegH);
  AssertTrue(ValDouble.Valid);
  AssertEquals(-12.505, ValDouble.Value, MARGIN);
end;


{ TTestTextConverter }

procedure TTestTextConverter.SetUp;
begin
  SetLength(Texts, 3);
  Converter := TTextConverter.Create;
  inherited SetUp;
end;

procedure TTestTextConverter.TearDown;
begin
  FreeAndNil(Converter);
  inherited TearDown;
end;

procedure TTestTextConverter.ThreeTexts2ThreeIntsHappyFlow;
begin
  Texts[0] := '13';
  Texts[1] := '22';
  Texts[2] := '8';
  IntResult := Converter.ThreeTexts2ThreeInts(Texts);
  AssertTrue(IntResult.Valid);
  AssertEquals(13, IntResult.ThreeInts[0]);
  AssertEquals(22, IntResult.ThreeInts[1]);
  AssertEquals(8, IntResult.ThreeInts[2]);
end;

procedure TTestTextConverter.ThreeTexts2ThreeIntsWrongLength;
var
  WrongTexts: array of string;
begin
  WrongTexts := ['13', '22'];
  IntResult := Converter.ThreeTexts2ThreeInts(WrongTexts);
  AssertFalse(IntResult.Valid);
end;

procedure TTestTextConverter.ThreeTexts2ThreeIntsNonNumeric;
begin
  Texts[0] := 'a';
  Texts[1] := '1';
  Texts[2] := '2';
  IntResult := Converter.ThreeTexts2ThreeInts(Texts);
  AssertFalse(IntResult.Valid);
end;

procedure TTestTextConverter.ThreeTexts2ThreeIntsUsingDoubles;
begin
  Texts[0] := '13.5';
  Texts[1] := '22';
  Texts[2] := '8';
  IntResult := Converter.ThreeTexts2ThreeInts(Texts);
  AssertFalse(IntResult.Valid);
end;

procedure TTestTextConverter.ThreeTexts2ThreeIntsUsingNegatives;
begin
  Texts[0] := '-13';
  Texts[1] := '22';
  Texts[2] := '8';
  IntResult := Converter.ThreeTexts2ThreeInts(Texts);
  AssertTrue(IntResult.Valid);
  AssertEquals(-13, IntResult.ThreeInts[0]);
  AssertEquals(22, IntResult.ThreeInts[1]);
  AssertEquals(8, IntResult.ThreeInts[2]);
end;

procedure TTestTextConverter.ThreeTexts2ThreeIntsUsingZeros;
begin
  Texts[0] := '13';
  Texts[1] := '0';
  Texts[2] := '0';
  IntResult := Converter.ThreeTexts2ThreeInts(Texts);
  AssertTrue(IntResult.Valid);
  AssertEquals(13, IntResult.ThreeInts[0]);
  AssertEquals(0, IntResult.ThreeInts[1]);
  AssertEquals(0, IntResult.ThreeInts[2]);
end;

initialization
  RegisterTest(TTestSexagesimalConverter);
  RegisterTest(TTestTextConverter);
  RegisterTest(TTestChartDataValidator);
  RegisterTest(TTestSettingsValidator);
end.

