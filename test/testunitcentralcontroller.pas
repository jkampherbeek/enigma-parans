{ Jan Kampherbeek, (c)  2021.
  Enigma Parans is open source.
  Please check the file copyright.txt in the root of the source for further details }

unit testunitcentralcontroller;

{$mode objfpc}{$H+}

interface

uses
  Classes, fpcunit, SysUtils, testregistry, unitcentralcontroller, unitdomainxchg;

type

  TTestChartData = class(TTestCase)
  protected
    ChartData: TChartData;
  published
    procedure ChartDataEasyFlow;
  end;

  { TTestCenCon }

  TTestCenCon = class(TTestCase)
  protected
    CenCon: TCenCon;
  published
    procedure CenConSingletonBehaviour;
    procedure CenConHandlingOfChartData;
    procedure CenConHandlingOfNilChartData;
  end;

implementation

const
  MARGIN = 0.00000001;

{ TTestCenCon }


procedure TTestCenCon.CenConSingletonBehaviour;
var
  CenCon1, CenCon2: TCenCon;
begin
  CenCon1 := TCenCon.Create;
  CenCon2 := TCenCon.Create;
  AssertTrue(CenCon1 = CenCon2);
end;

procedure TTestCenCon.CenConHandlingOfChartData;
var
  NameDescr: string;
  Longitude, Latitude, Altitude, JulianDay: double;
  ChartData: TChartData;
begin
  NameDescr := 'abc';
  Longitude := 30.5;
  Latitude := 40.4;
  JulianDay := 123456.789;
  Altitude:= 0.0;
  ChartData := TChartData.Create(NameDescr, Longitude, Latitude, Altitude, JulianDay);
  Cencon := TCenCon.Create;
  AssertFalse(CenCon.ChartDataDefined);
  CenCon.SetChartData(ChartData);
  AssertTrue(CenCon.ChartDataDefined);
end;

procedure TTestCenCon.CenConHandlingOfNilChartData;
var
  ChartData: TChartData;
begin
  Cencon := TCenCon.Create;
  ChartData := nil;
  CenCon.SetChartData(ChartData);
  AssertFalse(CenCon.ChartDataDefined);
end;


{ TTestChartData }

procedure TTestChartData.ChartDataEasyFlow;
var
  NameDescr: string;
  Longitude, Latitude, Altitude, JulianDay: double;
begin
  NameDescr := 'abc';
  Longitude := 30.5;
  Latitude := 40.4;
  Altitude:= 0.0;
  JulianDay := 123456.789;
  ChartData := TChartData.Create(NameDescr, Longitude, Latitude, Altitude, JulianDay);
  AssertEquals(NameDescr, ChartData.Namedescr);
  AssertEquals(Longitude, ChartData.Location.Longitude, MARGIN);
  AssertEquals(Latitude, ChartData.Location.Latitude, MARGIN);
  AssertEquals(JulianDay, ChartData.JulianDay);
end;



initialization
  RegisterTest(TTestCenCon);
  RegisterTest(TTestChartData);
end.

