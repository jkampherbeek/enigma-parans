{ Jan Kampherbeek, (c)  2021.
  Enigma Parans is open source.
  Please check the file copyright.txt in the root of the source for further details }

unit testunitformatting;

{$mode objfpc}{$H+}

interface

uses
  Classes, fpcunit, SysUtils, testregistry, unitformatting, unitdomainxchg;

type

  { TTestDmsGlyphPos }

  TTestDmsGlyphPos = class(TTestCase)
  protected
    DmsGlyphPos: TDmsGlyphPos;
    Hours, Degrees: double;
    ExpectedText, ExpectedGlyph: string;
    PositionTextWithGlyph: TPositionTextWithGlyph;
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestHappyFlow;
    procedure TestMinDegreesInSign;
    procedure TestMaxDegreesInSign;
  end;


  { TTestSexagesimalPos }

  TTestSexagesimalPos = class(TTestCase)
  protected
    SexagPos: TSexagesimalPos;
    Degrees, Hours: double;
    SizeIntegerPart: integer;
    SexagType: TEnumSexagType;
    Expected: string;
    procedure SetUp; override;
  procedure TearDown; override;
  published
    procedure TestHappyFlowDms;
    procedure TestHappyFlowTime;
    procedure TestIntegerSize3;
    procedure TestWrongIntegerSize;
    procedure TestWrongIntegerSize2Pos;
    procedure TestSmallValues;
    procedure TestNegativeValues;
    procedure TestNo60SecondsInDegrees;
    procedure TestNo60MinutesInDegrees;
    procedure TestNo60SecondsInTime;
    procedure TestNo60MinutesInTime;
  end;

implementation

{ TTestDmsGlyphPos }

procedure TTestDmsGlyphPos.SetUp;
begin
  DmsGlyphPos := TDmsGlyphPos.Create;
  inherited SetUp;
end;

procedure TTestDmsGlyphPos.TearDown;
begin
  FreeAndNil(DmsGlyphPos);
  inherited TearDown;
end;

procedure TTestDmsGlyphPos.TestHappyFlow;
begin
  Degrees:= 185.5;
  ExpectedText:= '05°30''00"';
  ExpectedGlyph:= '7';
  PositionTextWithGlyph:= DmsGlyphPos.FormatToTextAndGlyph(Degrees);
  AssertEquals(ExpectedText, PositionTextWithGlyph.PositionText);
  AssertEquals(ExpectedGlyph, PositionTextWithGlyph.Glyph);
end;

procedure TTestDmsGlyphPos.TestMinDegreesInSign;
begin
  Degrees:= 0.0;
  ExpectedText:= '00°00''00"';
  ExpectedGlyph:= '1';
  PositionTextWithGlyph:= DmsGlyphPos.FormatToTextAndGlyph(Degrees);
  AssertEquals(ExpectedText, PositionTextWithGlyph.PositionText);
  AssertEquals(ExpectedGlyph, PositionTextWithGlyph.Glyph);
end;

procedure TTestDmsGlyphPos.TestMaxDegreesInSign;
begin
  Degrees:= 239.9999999999999;   // 13 digits in fraction, max for a double is 15 à 16
  ExpectedText:= '29°59''59"';
  ExpectedGlyph:= '8';
  PositionTextWithGlyph:= DmsGlyphPos.FormatToTextAndGlyph(Degrees);
  AssertEquals(ExpectedText, PositionTextWithGlyph.PositionText);
  AssertEquals(ExpectedGlyph, PositionTextWithGlyph.Glyph);
end;


{ TTestSexagesimalPos }

procedure TTestSexagesimalPos.SetUp;
begin
  SexagPos := TSexagesimalPos.Create;
  inherited SetUp;
end;

procedure TTestSexagesimalPos.TearDown;
begin
  FreeAndNil(SexagPos);
  inherited TearDown;
end;

procedure TTestSexagesimalPos.TestHappyFlowDms;
begin
  Degrees := 20.5;
  SizeIntegerPart := 2;
  SexagType := dms;
  Expected := '20°30''00"';
  AssertEquals(Expected, SexagPos.FormatToText(Degrees, SizeIntegerPart, SexagType));
end;

procedure TTestSexagesimalPos.TestHappyFlowTime;
begin
  Hours := 11.5;
  SizeIntegerPart := 2;
  SexagType := time;
  Expected := '11:30:00';
  AssertEquals(Expected, SexagPos.FormatToText(Hours, SizeIntegerPart, SexagType));
end;

procedure TTestSexagesimalPos.TestIntegerSize3;
begin
  Degrees := 200.5;
  SizeIntegerPart := 3;
  SexagType := dms;
  Expected := '200°30''00"';
  AssertEquals(Expected, SexagPos.FormatToText(Degrees, SizeIntegerPart, SexagType));
end;

procedure TTestSexagesimalPos.TestWrongIntegerSize;
begin
  Degrees := 200.5;
  SizeIntegerPart := 5;
  SexagType := dms;
  Expected := '200°30''00"';
  AssertEquals(Expected, SexagPos.FormatToText(Degrees, SizeIntegerPart, SexagType));
end;

procedure TTestSexagesimalPos.TestWrongIntegerSize2Pos;
begin
  Degrees := 20.5;
  SizeIntegerPart := 5;
  SexagType := dms;
  Expected := '020°30''00"';
  AssertEquals(Expected, SexagPos.FormatToText(Degrees, SizeIntegerPart, SexagType));
end;

procedure TTestSexagesimalPos.TestSmallValues;
begin
  Degrees := 3.08416666666667;
  SizeIntegerPart := 3;
  SexagType := dms;
  Expected := '003°05''03"';
  AssertEquals(Expected, SexagPos.FormatToText(Degrees, SizeIntegerPart, SexagType));
end;

procedure TTestSexagesimalPos.TestNegativeValues;
begin
  Degrees := -40.5;
  SizeIntegerPart := 3;
  SexagType := dms;
  Expected := '-040°30''00"';
  AssertEquals(Expected, SexagPos.FormatToText(Degrees, SizeIntegerPart, SexagType));
end;


procedure TTestSexagesimalPos.TestNo60SecondsInTime;
begin
  Hours := 5.0 + 60/3600.0; // add 60 seconds
  SizeIntegerPart := 2;
  SexagType := time;
  Expected := '05:01:00';
  AssertEquals(Expected, SexagPos.FormatToText(Hours, SizeIntegerPart, SexagType));
end;

procedure TTestSexagesimalPos.TestNo60MinutesInTime;
begin
  Hours := 20.99999999999999;   // 14 digits, max size for double is 15 digits
  SizeIntegerPart := 2;
  SexagType := time;
  Expected := '20:59:59';
  AssertEquals(Expected, SexagPos.FormatToText(Hours, SizeIntegerPart, SexagType));
end;

procedure TTestSexagesimalPos.TestNo60SecondsInDegrees;
begin
  Degrees := 120.0 + 60/3600.0; // add 60 seconds
  SizeIntegerPart := 3;
  SexagType := dms;
  Expected := '120°01''00"';
  AssertEquals(Expected, SexagPos.FormatToText(Degrees, SizeIntegerPart, SexagType));
end;

procedure TTestSexagesimalPos.TestNo60MinutesInDegrees;
begin
  Degrees := 120.99999999999999;   // 14 digits, max size for double is 15 digits
  SizeIntegerPart := 3;
  SexagType := dms;
  Expected := '120°59''59"';
  AssertEquals(Expected, SexagPos.FormatToText(Degrees, SizeIntegerPart, SexagType));
end;



initialization
  RegisterTest(TTestDmsGlyphPos);
  RegisterTest(TTestSexagesimalPos);
end.

