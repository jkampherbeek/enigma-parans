{ Jan Kampherbeek, (c)  2021.
  Enigma Parans is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit TestUnitAstron;

{$mode objfpc}{$H+}

interface

uses
  Classes, fpcunit, SysUtils, testregistry, UnitAstron, UnitDomainXchg;

type

  { TestSeFrontend }

  TestSeFrontend = class(TTestCase)
  protected
    SeFrontend: TSeFrontend;
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestCalculateTrueObliquity;
    procedure TestCalculateJulDayNr;
    procedure TestCalculateHouseCusps;
    procedure TestCalculateCelestialPoint;
    procedure TestCalculateFixStar;
    procedure TestEcliptic2Equatorial;
    procedure TestEcliptic2Horizontal;
    procedure TestParanTimeFixStarRise;
    procedure TestParanTimeFixStarSet;
    procedure TestParanTimeFixStarCulminate;
    procedure TestParanTimeFixStarAntiCulminate;
    procedure TestCheckDateHappyFlow;
    procedure TestCheckDateError;
  end;

  { TestEphemeris }

  TestEphemeris = class(TTestCase)
  protected
    Ephemeris: TEphemeris;
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestPositionsForFixStar;
    procedure TestCheckDateHappyFlow;
    procedure TestCheckDateError;
    procedure TestCheckDateLeapYearGregOk;
    procedure TestCheckDateLeapYearJulFalse;
  end;

implementation

uses
  swissdelphi, UnitReferences;

const
  MARGIN: double = 0.00000001;



{ TestSeFrontend }

procedure TestSeFrontend.SetUp;
begin
  SeFrontend := TSeFrontend.Create;
  inherited SetUp;
end;

procedure TestSeFrontend.TearDown;
begin
  FreeAndNil(SeFrontend);
  inherited TearDown;
end;

procedure TestSeFrontend.TestCalculateTrueObliquity;
var
  JulianDay, Expected, Calculated: double;
begin
  JulianDay := 2459459.5;
  Expected := 23.4376111893246;
  Calculated := SeFrontend.CalculateTrueObliquity(JulianDay);
  AssertEquals(Expected, Calculated, MARGIN);
  JulianDay := 2451545.0;
  Expected := 23.437676716054849;
  Calculated := SeFrontend.CalculateTrueObliquity(JulianDay);
  AssertEquals(Expected, Calculated, MARGIN);
end;

procedure TestSeFrontend.TestCalculateJulDayNr;
var
  DateTime: TDateTime;
  Expected, Calculated: double;
begin
  DateTime.Year := 2000;
  DateTime.Month := 1;
  DateTime.Day := 1;
  DateTime.Ut := 12.0;
  DateTime.Calendar := 1;
  Expected := 2451545.0;
  Calculated := SeFrontend.CalculateJulDayNr(DateTime);
  AssertEquals(Expected, Calculated, MARGIN);
end;

procedure TestSeFrontend.TestCalculateHouseCusps;
var
  JulianDay, Expected: double;
  Location: TLocation;
  SeId: string;
  CalcResult: THouseCusps;
begin
  JulianDay := 2458372.33354167;
  Location.Longitude := 6.9;
  Location.Latitude := 52.2166667;
  SeId := 'P';    // Placidus
  CalcResult := SeFrontend.CalculateHouseCusps(JulianDay, Location, SeId);
  Expected := 79.8572331912747;
  AssertEquals(Expected, CalcResult.Cusps[2], MARGIN);
end;

procedure TestSeFrontend.TestCalculateCelestialPoint;
var
  JulianDay, ExpectedLon: double;
  Flags: longint;
  PlanetId: integer;
  Calculated: TDblArray;
begin
  Flags := SEFLG_SWIEPH or SEFLG_SPEED;
  PlanetId := SE_VENUS;
  JulianDay := 2458372.33354167;
  ExpectedLon := 211.0136487057;
  Calculated := SeFrontend.CalculateSolSysPoint(JulianDay, PlanetId, Flags);
  AssertEquals(ExpectedLon, Calculated[0]);
end;

procedure TestSeFrontend.TestCalculateFixStar;
var
  ExpectedLat, ExpectedLon, JulianDay: double;
  Calculated: TDblArray;
begin
  JulianDay := 2451545.0;    // 2000/1/1 12:00 UT
  ExpectedLon := 203.8361;
  ExpectedLat := -2.0543;
  Calculated := SeFrontend.CalculateFixStar('Spica,alVir', JulianDay);
  AssertEquals(ExpectedLon, Calculated[0]);
  AssertEquals(ExpectedLat, Calculated[1]);
end;


{ checked with formula's:
sin decl = cos lat . sin lon . sin obliq + sin lat . cos obliq
tan ra = (sin lon . cos obliq - tan lat . sin obliq) / cos lon      }
procedure TestSeFrontend.TestEcliptic2Equatorial;
var
  EclipticPos, EquatorialPos: TDblArray;
  Obliquity, ExpectedRA, ExpectedDecl: double;
begin
  EclipticPos:= [100.0, 1.0];
  Obliquity := 23.447;
  EquatorialPos := SeFrontend.Ecliptic2Equatorial(EclipticPos, Obliquity);
  ExpectedRA := 100.9616558824;
  ExpectedDecl := 24.0671636230;
  CheckEquals(ExpectedRA, EquatorialPos[0], MARGIN);
  CheckEquals(ExpectedDecl, EquatorialPos[1], MARGIN);
end;

procedure TestSeFrontend.TestEcliptic2Horizontal;
var
  JulianDay, ExpectedAzimuth, ExpectedAltitude: double;
  Location: TLocation;
  EclipticPos, HorizontalPos: TDblArray;
begin
  { Testdata:
    Location 52N13 6E54
    Date 2018-Oct-01, Time 0:00:00 UT
    Julian Day for ET: 2458392.5
    Celestial Object: Venus
    Azimuth according to Horizons: 327.1088     (-180 for SE version)
    Altitude according to Horizons: -55.8339,
    already calculated:
    longitude: 10 SC 24'26"    latitude -06 gr 49 m 16 s
    220.4072222222   /  -6.8211111111                      }

  JulianDay := 2458392.5;
  EclipticPos := [220.4072222222, -6.8211111111];
  Location.Longitude := 6.9;
  Location.Latitude := 52.2166667;
  HorizontalPos := SeFrontend.Ecliptic2Horizontal(EclipticPos, Location, JulianDay);
  ExpectedAzimuth := 147.10816175;
  ExpectedAltitude := -55.83008776;
  AssertEquals(ExpectedAzimuth, HorizontalPos[0], MARGIN);
  AssertEquals(ExpectedAltitude, HorizontalPos[1], MARGIN);
end;

procedure TestSeFrontend.TestParanTimeFixStarRise;
var
  StarName: string;
  CalcResult, Expected, JulianDay: double;
  Location: TLocation;
begin
  StarName := 'Spica,alVir';
  JulianDay := 2451545.0;    // 2000/1/1 12:00 UT
  Location.Longitude := 6.9;
  Location.Latitude := 52.2166667;
  CalcResult := SeFrontEnd.ParanTimeFixStar(StarName, rising, JulianDay, Location);
  Expected := 2451545.5504;
  AssertEquals(Expected, CalcResult);           { TODO : Check result of unittest    }
end;

procedure TestSeFrontend.TestParanTimeFixStarSet;
var
  StarName: string;
  CalcResult, Expected, JulianDay: double;
  Location: TLocation;
begin
  StarName := 'Spica,alVir';
  JulianDay := 2451545.0;    // 2000/1/1 12:00 UT
  Location.Longitude := 6.9;
  Location.Latitude := 52.2166667;
  CalcResult := SeFrontEnd.ParanTimeFixStar(StarName, setting, JulianDay, Location);
  Expected := 2451545.9673;
  AssertEquals(Expected, CalcResult);                           { TODO : Check result of unittest    }
end;

procedure TestSeFrontend.TestParanTimeFixStarCulminate;
var
  StarName: string;
  CalcResult, Expected, JulianDay: double;
  Location: TLocation;
begin
  StarName := 'Spica,alVir';
  JulianDay := 2451545.0;    // 2000/1/1 12:00 UT
  Location.Longitude := 6.9;
  Location.Latitude := 52.2166667;
  CalcResult := SeFrontEnd.ParanTimeFixStar(StarName, culminating, JulianDay, Location);
  Expected := 2451545.7588573736;
  AssertEquals(Expected, CalcResult);

end;

procedure TestSeFrontend.TestParanTimeFixStarAntiCulminate;
var
  StarName: string;
  CalcResult, Expected, JulianDay: double;
  Location: TLocation;
begin
  StarName := 'Spica,alVir';
  JulianDay := 2451545.0;    // 2000/1/1 12:00 UT
  Location.Longitude := 6.9;
  Location.Latitude := 52.2166667;
  CalcResult := SeFrontEnd.ParanTimeFixStar(StarName, anticulminating, JulianDay, Location);
  Expected := 2451545.2602225989;
  AssertEquals(Expected, CalcResult);
end;

procedure TestSeFrontend.TestCheckDateHappyFlow;
var
  Year, Month, Day: integer;
  GregFlag: char;
begin
  Year := 1953;
  Month := 1;
  Day := 29;
  GregFlag := 'g';
  AssertTrue(SeFrontend.CheckDate(Year, Month, Day, GregFlag).Valid);
end;

procedure TestSeFrontend.TestCheckDateError;
var
  Year, Month, Day: integer;
  GregFlag: char;
  ValDouble: TValidatedDouble;
begin
  Year := 1953;
  Month := 1;
  Day := 32;
  GregFlag := 'g';
  ValDouble := SeFrontend.CheckDate(Year, Month, Day, GregFlag);
  AssertFalse(ValDouble.Valid);
end;


{ TestEphemeris }

procedure TestEphemeris.SetUp;
begin
  Ephemeris := TEphemeris.Create;
  inherited SetUp;
end;

procedure TestEphemeris.TearDown;
begin
  FreeAndNil(Ephemeris);
  inherited TearDown;
end;

procedure TestEphemeris.TestPositionsForFixStar;
var
  CalcResult: TFullParanPos;
  Star: TFixStarSpec;
  JulianDay, ShiftedJd, Obliquity, ExpectedLon, ExpectedLat, ExpectedRiseTime, ExpectedAntiCulmTime,
  ExpectedDecl, ExpectedAzim: double;
  Location: TLocation;
  ParanRefs: TParanRefs;
begin
  ParanRefs:= TParanRefs.Create;
  Star:= ParanRefs.AllFixStarSpecs[7];   // Spica
  JulianDay := 2451545.0;                // 2000/1/1 12:00 UT
  ShiftedJd := JulianDay;
  Obliquity := 23.437676716054849;
  Location.Longitude := 6.9;
  Location.Latitude := 52.2166667;
  CalcResult := Ephemeris.PositionsForFixStar(Star, JulianDay, ShiftedJd, Obliquity, Location);
  ExpectedLon := 203.8361481472;
  ExpectedLat := -2.054285399;
  ExpectedDecl := -11.158613669143;
  ExpectedAzim := 80.044495694949;
  ExpectedRiseTime := 2451545.55037586;
  ExpectedAntiCulmTime := 2451545.2602225989;
  AssertEquals(ExpectedLon, CalcResult.FullEclPos[0], MARGIN);
  AssertEquals(ExpectedLat, CalcResult.FullEclPos[1], MARGIN);
  AssertEquals(ExpectedDecl, CalcResult.EquatorialPos[1], MARGIN);
  AssertEquals(ExpectedAzim, CalcResult.HorizontalPos[0], MARGIN);
  AssertEquals(ExpectedRiseTime, CalcResult.ParanTimes[0], MARGIN);
  AssertEquals(ExpectedAntiCulmTime, CalcResult.ParanTimes[3], MARGIN);
end;

procedure TestEphemeris.TestCheckDateHappyFlow;
var
  Year, Month, Day: integer;
  Greg: boolean;
begin
  Year := 1953;
  Month := 1;
  Day := 29;
  Greg := True;
  AssertTrue(Ephemeris.CheckDate(Year, Month, Day, Greg).Valid);
end;

procedure TestEphemeris.TestCheckDateError;
var
  Year, Month, Day: integer;
  Greg: boolean;
begin
  Year := 1953;
  Month := 13;
  Day := 29;
  Greg := True;
  AssertFalse(Ephemeris.CheckDate(Year, Month, Day, Greg).Valid);
end;

procedure TestEphemeris.TestCheckDateLeapYearGregOk;
var
  Year, Month, Day: integer;
  Greg: boolean;
begin
  Year := 2000;
  Month := 2;
  Day := 29;
  Greg := True;
  AssertTrue(Ephemeris.CheckDate(Year, Month, Day, Greg).Valid);
end;

procedure TestEphemeris.TestCheckDateLeapYearJulFalse;
var
  Year, Month, Day: integer;
  Greg: boolean;
begin
  Year := 2101;
  Month := 2;
  Day := 29;
  Greg := False;
  AssertFalse(Ephemeris.CheckDate(Year, Month, Day, Greg).Valid);
end;


initialization
  RegisterTest(TestSeFrontend);
  RegisterTest(TestEphemeris);
end.
