{ Jan Kampherbeek, (c)  2021.
  Enigma Parans is open source.
  Please check the file copyright.txt in the root of the source for further details }
program parans;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, unitstart, UnitAstron, swissdelphi, UnitLog, UnitDataInput,
  unittranslation, unitconst, unitfesupport, unitcentralcontroller,
  Unitcentralform, unitformatting, unitanalysis, UnitReferences, UnitAPI,
  unithandlers, unitsettings, unitabout
  { you can add units after this };

{$R *.res}

begin
  RequireDerivedFormResource:=True;
  Application.Scaled:=True;
  Application.Initialize;
  Application.CreateForm(TFormDataInput, FormDataInput);
  Application.CreateForm(TFormCentral, FormCentral);
  Application.CreateForm(TFormSettings, FormSettings);
  Application.CreateForm(TFormAbout, FormAbout);
  Application.Run;
end.

