{ Jan Kampherbeek, (c)  2021.
  Enigma Parans is open source.
  Please check the file copyright.txt in the root of the source for further details }

unit UnitAstron;

{< Astronomical calculations.}

{$mode objfpc}{$H+}

interface

uses
  Classes, swissdelphi, SysUtils, unitdomainxchg, UnitLog;

type

  { Frontend for the Swiss Ephemeris. Has direct access to the SE using the methods as defined in swissdelphi.pas }

  { TSeFrontend }

  TSeFrontend = class
  private
    Flags: longint;
    Logger: TLogger;
    function ParanTime(PStarName: string; PCpId: integer; PParanPos: TEnumParanPositions;
      PJulianDay: double; PLocation: TLocation): double;
  public
    constructor Create;
    procedure SetTopo(PLocation: TLocation);
    function CalculateTrueObliquity(PJulianDay: double): double;
    function CalculateJulDayNr(PDateTime: TDateTime): double;
    function CalculateHouseCusps(PJulianDay: double; PLocation: TLocation; PSeId: string): THouseCusps;
    function CalculateSolSysPoint(PJulianDay: double; PSeId: integer; PFlags: longint): TDblArray;
    function CalculateFixStar(PStarName: string; PJdNr: double): TDblArray;
    function Ecliptic2Equatorial(PEclipticPos: array of double; PObliquity: double): TDblArray;
    function Ecliptic2Horizontal(PEclipticPos: array of double; PLocation: TLocation;
      PJulianDay: double): TDblArray;
    function ParanTimeFixStar(PStarName: string; PParanPos: TEnumParanPositions; PShiftedJd: double;
      PLocation: TLocation): double;
    function ParanTimeSolSysPoint(PSsId: integer; PParanPos: TEnumParanPositions; PShiftedJd: double;
      PLocation: TLocation): double;
    function CheckDate(PYear, PMonth, PDay: integer; PGregFlag: char): TValidatedDouble;
    function ConstructDateTime(PJulianDay: double; PGregFlag: integer): TDateTime;
    function ConstructFullDateTime(PJulianDay, PTzOffset: double; PDst, PGregCal: boolean): TFullDateTime;
  end;


  { Accesspoint for astronomical calculations. Uses SeFrontend to perform the calculations. }

  { TEphemeris }

  TEphemeris = class
  strict private
    SeFrontend: TSeFrontend;
  public
    constructor Create;
    destructor Destroy; override;
    procedure PrepareTopocentric(PLocation: TLocation);
    function PositionsForFixStar(PFixStar: TFixStarSpec; PJulianDay, PShiftedJd, Obliquity: double;
      PLocation: TLocation): TFullParanPos;
    function PositionsForSolSysPoint(PSolSysPoint: TSolSysPointSpec; PJulianDay, PShiftedJd, Obliquity: double;
      PLocation: TLocation): TFullParanPos;
    function PositionsHouses(PHouseSystem: string; PJulianDay: double; PLocation: TLocation): THouseCUsps;
    function JulianDayNumber(PDateTime: TDateTime): double;
    function ShiftedJulianDay(PJulianDay: double; PLocation: TLocation): double;
    function Obliquity(PJulianDay: double): double;
    function CheckDate(PYear, PMonth, PDay: integer; GregCal: boolean): TValidatedDouble;
    function ConstructDateTime(PJulianDay: double; PGregCal: boolean): TDateTime;
    function ConstructFullDateTime(PJulianDay, PTzOffset: double; PDst, PGregCal: boolean): TFullDateTime;
  end;


implementation

uses
  unitconst;

{ TEphemeris }

constructor TEphemeris.Create;
begin
  SeFrontend := TSeFrontend.Create;
end;

destructor TEphemeris.Destroy;
begin
  FreeAndNil(SeFrontend);
  inherited Destroy;
end;

procedure TEphemeris.PrepareTopocentric(PLocation: TLocation);
begin
  SeFrontend.SetTopo(PLocation);
end;

function TEphemeris.PositionsForFixStar(PFixStar: TFixStarSpec; PJulianDay, PShiftedJd, Obliquity: double;
  PLocation: TLocation): TFullParanPos;
var
  ParPosTime, FullEclipticPos, EquatorialPos, HorizontalPos: TDblArray;
  StarName: string;
begin
  StarName := PFixStar.SearchArg;
  FullEclipticPos := SeFrontend.CalculateFixStar(StarName, PJulianDay);
  EquatorialPos := SeFrontend.Ecliptic2Equatorial(FullEclipticPos, Obliquity);
  HorizontalPos := SeFrontend.Ecliptic2Horizontal(FullEclipticPos, PLocation, PJulianDay);
  ParPosTime := [SeFrontEnd.ParanTimeFixStar(StarName, rising, PShiftedJd, PLocation),
    SeFrontEnd.ParanTimeFixStar(StarName, culminating, PShiftedJd, PLocation),
    SeFrontEnd.ParanTimeFixStar(StarName, setting, PShiftedJd, PLocation),
    SeFrontEnd.ParanTimeFixStar(StarName, anticulminating, PShiftedJd, PLocation)];
  Result := TFullParanPos.Create(PFixStar, FullEclipticPos, EquatorialPos, HorizontalPos, ParPosTime);
end;

function TEphemeris.PositionsForSolSysPoint(PSolSysPoint: TSolSysPointSpec;
  PJulianDay, PShiftedJd, Obliquity: double; PLocation: TLocation): TFullParanPos;
var
  ParPosTime, FullEclipticPos, EquatorialPos, HorizontalPos: TDblArray;
  Flags: longint;
  SsId: integer;
begin
  SsId := PSolSysPoint.SeId;
  Flags := 2 or 256;           // Swiss Ephemeris and speed
  FullEclipticPos := SeFrontend.CalculateSolSysPoint(PJulianDay, SsId, Flags);
  EquatorialPos := SeFrontend.Ecliptic2Equatorial(FullEclipticPos, Obliquity);
  HorizontalPos := SeFrontend.Ecliptic2Horizontal(FullEclipticPos, PLocation, PJulianDay);
  ParPosTime := [SeFrontEnd.ParanTimeSolSysPoint(SsId, rising, PShiftedJd, PLocation),
    SeFrontEnd.ParanTimeSolSysPoint(SsId, culminating, PShiftedJd, PLocation),
    SeFrontEnd.ParanTimeSolSysPoint(SsId, setting, PShiftedJd, PLocation),
    SeFrontEnd.ParanTimeSolSysPoint(SsId, anticulminating, PShiftedJd, PLocation)];
  Result := TFullParanPos.Create(PSolSysPoint, FullEclipticPos, EquatorialPos, HorizontalPos, ParPosTime);
end;

function TEphemeris.PositionsHouses(PHouseSystem: string; PJulianDay: double; PLocation: TLocation): THouseCUsps;
begin
  Result := SeFrontend.CalculateHouseCusps(PJulianDay, PLocation, PHouseSystem);
end;

function TEphemeris.JulianDayNumber(PDateTime: TDateTime): double;
begin
  Result := SeFrontend.CalculateJulDayNr(PDateTime);
end;

function TEphemeris.ShiftedJulianDay(PJulianDay: double; PLocation: TLocation): double;
var
  ShiftedJd, TempJd, TempJdPrevDay: double;
  SsId: integer;
begin
  SsId := 0;  // Sun
  ShiftedJd := PJulianDay;
  TempJd := SeFrontend.ParanTimeSolSysPoint(SsId, rising, ShiftedJd, PLocation);
  TempJdPrevDay := SeFrontend.ParanTimeSolSysPoint(SsId, rising, ShiftedJd - 1, PLocation);
  if (TempJd <= ShiftedJd) then Result := ShiftedJd
  else
    Result := TempJdPrevDay - 0.000000000001;
end;

function TEphemeris.Obliquity(PJulianDay: double): double;
begin
  Result := SeFrontend.CalculateTrueObliquity(PJulianDay);
end;

function TEphemeris.CheckDate(PYear, PMonth, PDay: integer; GregCal: boolean): TValidatedDouble;
var
  GregorianFlag: char;
begin
  if (GregCal) then GregorianFlag := 'g'
  else
    GregorianFlag := 'j';
  Result := SeFrontend.CheckDate(PYear, PMonth, PDay, GregorianFlag);
end;

function TEphemeris.ConstructDateTime(PJulianDay: double; PGregCal: boolean): TDateTime;
var
  GregFlag: integer;
begin
  if (PGregCal) then GregFlag := 1
  else
    GregFlag := 0;
  Result := SeFrontend.ConstructDateTime(PJulianDay, GregFlag);
end;

function TEphemeris.ConstructFullDateTime(PJulianDay, PTzOffset: double; PDst, PGregCal: boolean): TFullDateTime;
begin
  Result := SeFrontend.ConstructFullDateTime(PJulianDay, PTzOffset, PDst, PGregCal);
end;

{ TSeFrontend }

constructor TSeFrontend.Create;
begin
  Logger := TLogger.Create;
  swe_set_ephe_path('..//se');
  Flags := 2 or 256;           // Swiss Ephemeris and speed
end;

procedure TSeFrontend.SetTopo(PLocation: TLocation);
begin
  swe_set_topo(PLocation.Longitude, PLocation.Latitude, PLocation.Altitude);
end;

function TSeFrontend.CalculateTrueObliquity(PJulianDay: double): double;
var
  Positions: array[0..6] of double;
  CalcResult: longint;
  ErrorText: array[0..255] of char;
  i: integer;
begin
  for i := Low(Positions) to High(Positions) do Positions[i] := 0.0;
  CalcResult := swe_calc_ut(PJulianDay, SE_ECL_NUT, 0, Positions[0], ErrorText);
  if (CalcResult < 0) then Logger.Log('Error in TSeFrontend.CalculateObliquity: ' + ErrorText);
  Result := Positions[0];
end;

function TSeFrontend.CalculateJulDayNr(PDateTime: TDateTime): double;
begin
  with PDateTime do Result := swe_julday(Year, Month, Day, Ut, Calendar);
end;

function TSeFrontend.CalculateHouseCusps(PJulianDay: double; PLocation: TLocation; PSeId: string): THouseCusps;
var
  i, CalcResult: integer;
  SeChar: AnsiChar;
  HouseValues, HouseDeclValues: array[0..12] of double;
  AscMcValues: array[0..10] of double;
  AscDecl, McDecl: double;
begin
  for i := Low(AscMcValues) to High(AscMcValues) do AscMcValues[i] := 0.0;
  for i := Low(HouseValues) to High(HouseValues) do HouseValues[i] := 0.0;
  for i := Low(HouseDeclValues) to High(HouseDeclValues) do HouseDeclValues[i] := 0.0;
  SeChar := AnsiChar(PSeId[1]);
  CalcResult := swe_houses(PJulianDay, PLocation.Latitude, PLocation.Longitude, SeChar,
    HouseValues[0], AscMcValues[0]);
  if (CalcResult < 0) then Logger.Log('Error in TSeFrontend.CalculateHouseCusps. CalcResult: ' +
      IntToStr(CalcResult));
  AscDecl := 0.0;
  McDecl := 0.0;
  Result := THouseCusps.Create(AscMcValues[1], AscMcValues[0], McDecl, AscDecl, HouseValues, HouseDeclValues);
end;

function TSeFrontend.CalculateSolSysPoint(PJulianDay: double; PSeId: integer; PFlags: longint): TDblArray;
var
  Positions: array[0..6] of double;
  CalcResult: longint;
  i: integer;
  ErrorText: array[0..255] of char;
begin
  for i := Low(Positions) to High(Positions) do Positions[i] := 0.0;
  CalcResult := swe_calc_ut(PJulianDay, PSeId, PFlags, Positions[0], ErrorText);
  if (CalcResult < 0) then Logger.Log('Error in TSeFrontend.CalculateCelestialPoint: ' + ErrorText);
  Result := Positions;
end;

function TSeFrontend.CalculateFixStar(PStarName: string; PJdNr: double): TDblArray;
var
  Positions: array[0..6] of double;
  CalcResult: longint;
  i: integer;
  ErrorText: array[0..255] of char;
  StarName: array[0..40] of char;
begin
  for i := Low(Positions) to High(Positions) do Positions[i] := 0.0;
  StarName := PAnsiChar(PStarName);
  CalcResult := swe_fixstar2_ut(StarName, PJdNr, Flags, Positions[0], ErrorText);
  if (CalcResult < 0) then Logger.Log('Error in TSeFrontend.CalculateFixStar: ' + ErrorText);
  Result := Positions;
end;

function TSeFrontend.Ecliptic2Equatorial(PEclipticPos: array of double; PObliquity: double): TDblArray;
var
  EclipticValues, EquatorialValues: array of double;
  NegativeObliquity: double;  // for conversion ecliptic to equatorial, obliquity must be negative.
  EquatorialPos: TDblArray;
begin
  EclipticValues := [PEclipticPos[0], PEclipticPos[1], 1.0];   // distance can be ignored, 1.0 is a placeholder.
  EquatorialValues := [0.0, 0.0, 0.0];
  NegativeObliquity := -(Abs(PObliquity));
  swe_cotrans(EclipticValues[0], EquatorialValues[0], NegativeObliquity);
  EquatorialPos := [EquatorialValues[0], EquatorialValues[1]];
  Result := EquatorialPos;
end;

function TSeFrontend.Ecliptic2Horizontal(PEclipticPos: array of double; PLocation: TLocation;
  PJulianDay: double): TDblArray;
var
  EclipticValues, HorizontalValues, LocationValues: array of double;
  Flag: longint;
  AtmosphPressure, AtmosphTemperature: double;
  HorizontalPos: TDblArray;
begin
  Flag := SE_ECL2HOR;
  AtmosphPressure := 0.0;      // not relevant for true altitude
  AtmosphTemperature := 0.0;   // not relevant for true altitude
  HorizontalPos := [0.0, 0.0];
  EclipticValues := [PEclipticPos[0], PEclipticPos[1], 0.0];
  LocationValues := [PLocation.Longitude, PLocation.Latitude, PLocation.Altitude];
  HorizontalValues := [0.0, 0.0, 0.0];
  swe_azalt(PJulianDay, Flag, LocationValues[0], AtmosphPressure, AtmosphTemperature,
    EclipticValues[0], HorizontalValues[0]);
  HorizontalPos[0] := HorizontalValues[0];
  HorizontalPos[1] := HorizontalValues[1];
  Result := HorizontalPos;
end;

function TSeFrontend.ParanTime(PStarName: string; PCpId: integer; PParanPos: TEnumParanPositions;
  PJulianDay: double; PLocation: TLocation): double;
var
  i, CalcResult: integer;
  PlanetId: integer;   // is not used but must be defined
  Rsmi: integer;       // Rise 1, Set 2, Culm 4, Anticulm 8
  Flag: longint;
  ErrorText: array[0..255] of char;
  StarName: array[0..40] of char;
  LocationValues: array of double;
  ParanValues: array[0..9] of double;
  AtmosphPressure, AtmosphTemperature: double;
begin
  if (Trim(PStarName) <> '') then begin
    StarName := PAnsiChar(PStarName);
    PlanetId := -1;
  end else begin
    StarName := '';
    PlanetId := PCpId;
  end;
  Flag := SEFLG_SWIEPH;
  AtmosphPressure := 0.0;
  AtmosphTemperature := 0.0;
  case PParanPos of
    rising: Rsmi := 1 or SE_BIT_DISC_CENTER or SE_BIT_NO_REFRACTION;
    setting: Rsmi := 2 or SE_BIT_DISC_CENTER or SE_BIT_NO_REFRACTION;
    culminating: Rsmi := 4 or SE_BIT_DISC_CENTER or SE_BIT_NO_REFRACTION;
    anticulminating: Rsmi := 8 or SE_BIT_DISC_CENTER or SE_BIT_NO_REFRACTION;
  end;
  for i := 0 to 9 do ParanValues[i] := 0.0;
  LocationValues := [PLocation.Longitude, PLocation.Latitude, PLocation.Altitude];
  CalcResult := swe_rise_trans(PJulianDay, PlanetId, StarName, Flag, Rsmi, LocationValues[0],
    AtmosphPressure, AtmosphTemperature, ParanValues[0], ErrorText);
  if (CalcResult < 0) then Logger.Log('Error in TSeFrontend.ParanTime: ' + ErrorText);
  Result := ParanValues[0];
end;

function TSeFrontend.ParanTimeFixStar(PStarName: string; PParanPos: TEnumParanPositions;
  PShiftedJd: double; PLocation: TLocation): double;
var
  CpId: integer;
begin
  CpId := -1;
  Result := ParanTime(PStarName, CpId, PParanPos, PShiftedJd, PLocation);
end;

function TSeFrontend.ParanTimeSolSysPoint(PSsId: integer; PParanPos: TEnumParanPositions;
  PShiftedJd: double; PLocation: TLocation): double;
var
  StarName: string;
begin
  Starname := '';
  Result := ParanTime(StarName, PSsId, PParanPos, PShiftedJd, PLocation);
end;

function TSeFrontend.CheckDate(PYear, PMonth, PDay: integer; PGregFlag: char): TValidatedDouble;
var
  ReturnStatus: integer;
  JulianDay: double = 0.0;
  ValidFlag: boolean;
  ValidatedDouble: TValidatedDouble;
begin
  ReturnStatus := swe_date_conversion(PYear, PMonth, PDay, 0.0, PGregFlag, JulianDay);
  ValidFlag := (ReturnStatus = 0) and (JulianDay >= MIN_JD_ASTRON) and (JulianDay <= MAX_JD);
  ValidatedDouble.Valid := ValidFlag;
  ValidatedDouble.Value := JulianDay;
  Result := ValidatedDouble;
end;

function TSeFrontend.ConstructDateTime(PJulianDay: double; PGregFlag: integer): TDateTime;
var
  year: integer = 0;
  month: integer = 0;
  day: integer = 0;
  hour: double = 0.0;
  DateTime: TDateTime;
begin
  swe_revjul(PJulianDay, PGregFlag, year, month, day, hour);
  Datetime.Year := year;
  DateTime.Month := month;
  DateTime.Day := day;
  DateTime.Ut := hour;
  DateTime.Calendar := PGregFlag;
  Result := DateTime;
end;

function TSeFrontend.ConstructFullDateTime(PJulianDay, PTzOffset: double; PDst, PGregCal: boolean): TFullDateTime;
begin
  Result := TFullDatetime.Create(PJulianDay, PTzOffset, PDst, PGregcal);
end;


end.
