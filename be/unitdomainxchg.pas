{ Jan Kampherbeek, (c)  2021.
  Enigma Parans is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit UnitDomainXchg;

{< Domain items that can be used by the frontend and the backend.}

{$mode objfpc}{$H+}

interface



uses
  Classes, inifiles, SysUtils;

type

  TFullParanPos = class;

  TIntArray = array of integer;
  TDblArray = array of double;
  TFullParanPosArray = array of TFullParanPos;

  TEnumClassicalPlanets = (sun, moon, mercury, venus, mars, jupiter, saturn);
  TEnumModernPlanets = (uranus, neptune, pluto);
  TEnumParanPositions = (rising, culminating, setting, anticulminating);
  TEnumSolSysPointSets = (classical, modern);
  TEnumFixStarSets = (ptolemy, brady);

  { TSettings }

  TSettings = class
  strict private
    FModernPlanets: boolean;
    FOrbSeconds: integer;
    procedure ReadFromIni;
  public
    constructor Create(PModernPlanets: boolean; POrbSeconds: integer);
    constructor Create;
    procedure Persist;
    property ModernPlanets: boolean read FModernPlanets;
    property OrbSeconds: integer read FOrbSeconds;
  end;


  { TInputValues }
  { Inputvalues to be used for calculating local clocktime from jd. }
  TInputValues = class
  strict private
    FTimezoneOffset: double;
    FDst, FGregCal: boolean;
  public
    constructor Create(PTimezoneOffset: double; PDst, PGregCal: boolean);
    property TimezoneOffset: double read FTimezoneOffset;
    property Dst: boolean read FDst;
    property GregCal: boolean read FGregCal;
  end;

  TPresentableInputData = record
    NameDescr, Location, DateTime: string;
  end;

  TMonthSpec = record
    Index: integer;
    Name: string;
  end;

  TMonthSpecArray = array of TMonthSpec;

  TCalendarSpec = record
    Index: integer;
    Name: string;
  end;

  TCalendarSpecArray = array of TCalendarSpec;

  TYearCountSpec = record
    Index: integer;
    Name: string;
  end;

  TYearCountSpecArray = array of TYearCountSpec;

  TGeoDirSpec = record
    PlusMinus: integer;
    Name: string;
  end;

  TGeoDirSpecArray = array of TGeoDirSpec;

  TPositionTextWithGlyph = record
    PositionText, Glyph: string;
  end;


  { TCelPointSpec }
  { Parent for objects that represent celestial points.
    Id is a unique identifier, Name is the argument to retrieve the translation. }
  TCelPointSpec = class
  protected
    FId: integer;
    FName: string;
  public
    constructor Create(PId: integer; PName: string);
    property Id: integer read FId;
    property Name: string read FName;
  end;

  { TFixStarSpec }
  { Represents a fix star.
    Id is a unique identifier, Name is the argument to retrieve the translation,
    FormalName is the official astronomical name, SearchArg an argument to locate the definition in the SE. }
  TFixStarSpec = class(TCelPointSpec)
  strict private
    FFormalName, FSearchArg: string;
  public
    constructor Create(PId: integer; PName, PFormalName, PSearchArg: string);
    property FormalName: string read FFormalName;
    property SearchArg: string read FSearchArg;
  end;

  TFixStarSpecArray = array of TFixStarSpec;

  { TSolSysPointSpec }
  { Represents a point in the solar system.
    Id is a unique identifier, SeId the identifier as used in the SE, Name is the argument to retrieve the translation. }
  TSolSysPointSpec = class(TCelPointSpec)
  strict private
    FSeId: integer;
  public
    constructor Create(PId, PSeId: integer; PName: string);
    property SeId: integer read FSeId;
  end;

  TSolSysPointSpecArray = array of TSolSysPointSpec;

  { TTimeZoneSpec }
  { A time zone.
    Name is the argument to retrieve the translation, Offset is the difference with UT (East is +, West is - }
  TTimeZoneSpec = class
  strict private
    FOffset: double;
    FName: string;
  public
    constructor Create(PName: string; POffset: double);
    property Offset: double read FOffset;
    property Name: string read FName;
  end;

  TTimeZoneSpecArray = array of TTimeZoneSpec;

  { THouseCusps }
  { Longitude and declination for MC, Ascendant and cusps.}
  THouseCusps = class
  strict private
    FMc, FAsc, FMcDecl, FAscDecl: double;
    FCusps, FCuspsDecl: TDblArray;
  public
    constructor Create(PMc, PAsc, PMcDecl, PAscDecl: double; PCusps, PCuspsDecl: TDblArray);
    property Mc: double read FMc;
    property Asc: double read FAsc;
    property McDecl: double read FMcDecl;
    property AscDecl: double read FAscDecl;
    property Cusps: TDblArray read FCusps;
    property CuspsDecl: TDblArray read FCuspsDecl;
  end;

  { TFullParanPos }
  { All positions for a paran:
    FullEclPos contains longitude, latitude, distance, speed in lon, speed in lat, speed in dist, in that sequence.
    EquatorialPos contains right ascension and declination in that sequence.
    HorizontalPos contains azimuth and altitude in that sequence.
    ParanTimes contains times for rising, culminating, setting and anti-cilminating, in that sequence.}
  TFullParanPos = class
  strict private
    FCelPointSpec: TCelPointSpec;
    FFullEclPos, FEquatorialPos, FHorizontalPos, FParanTimes: TDblArray;
  public
    constructor Create(PCelPointSpec: TCelPointSpec;
      PFullEclPos, PEquatorialPos, PHorizontalPos, PParanTimes: TDblArray);
    property CelPointSpec: TCelPointSpec read FCelPointSpec;
    property FullEclPos: TDblArray read FFullEclPos;
    property EquatorialPos: TDblArray read FEquatorialPos;
    property HorizontalPos: TDblArray read FHorizontalPos;
    property ParanTimes: TDblArray read FParanTimes;
  end;

  { TParanData }
  { Paran times, defined as Julian day number, for a celestial point. }
  TParanData = class
  strict private
    FCelPointSpec: TCelPointSpec;
    FJulianDay: double;
    FParanPos: TEnumParanPositions;
  public
    constructor Create(PCelPointSpec: TCelPointSpec; PJulianDay: double; PParanPos: TEnumParanPositions);
    property CelPointSpec: TCelPointSpec read FCelPointSpec;
    property JulianDay: double read FJulianDay;
    property ParanPos: TEnumParanPositions read FParanPos;
  end;

  TParanDataArray = array of TParanData;

  { TParanMatches }
  { Matches in paran times for a specific celestial point (Base) with a set of celestial points (Matches) }
  TParanMatches = class
  strict private
    FBase: TParanData;
    FMatches: TParanDataArray;
  public
    constructor Create(PBase: TParanData; PMatches: TParanDataArray);
    property Base: TParanData read FBase;
    property Matches: TParanDataArray read FMatches;
  end;

  TParanMatchesArray = array of TParanMatches;

  TLocation = record
    Longitude, Latitude, Altitude: double;   // Altitude in meters above sealevel
  end;

  TDateTime = record
    Year, Month, Day: integer;    // Year in astronomical year-count
    Ut: double;
    Calendar: integer;            // 0 is Julian, 1 is Gregorian
  end;

  TValidatedDouble = record
    Valid: boolean;
    Value: double;
  end;

  TValidatedInteger = record
    Valid: boolean;
    Value: integer;
  end;


  { TFullDatetime }

  TFullDatetime = class
  private
    FYear, FMonth, FDay, FHour, FMinute, FSecond: integer;
    FJulianDay, FClockTime, FTzOffset: double;
    FDst, FGregCal: boolean;
    FFormattedText: string;
    FFormattedMDTime: string;
    procedure ConstructFormattedTexts;
    procedure DefineUt;
  public
    constructor Create(PYear, PMonth, PDay, PHour, PMinute, PSecond: integer; PTzOffset: double;
      PDst, PGregCal: boolean);
    constructor Create(PJulianDay, PTzOffset: double; PDst, PGregCal: boolean);
    property Year: integer read FYear;
    property Month: integer read FMonth;
    property Day: integer read FDay;
    property Hour: integer read FHour;
    property Minute: integer read FMinute;
    property Second: integer read FSecond;
    property JulianDay: double read FJulianDay;
    property ClockTime: double read FClockTime;
    property TzOffset: double read FTzOffset;
    property Dst: boolean read FDst;
    property GregCal: boolean read FGregCal;
    property FormattedText: string read FFormattedText;
    property FormattedMDTime: string read FFormattedMDTime;
  end;

  { TParanCalculationResult }
  { Calculated parans and housecusps. }
  TParanCalculationResult = class
  strict private
    FHouseCusps: THouseCusps;
    FSolSysPoints, FFixstars: TFullParanPosArray;
  public
    constructor Create(PSolSysPoints, PFixStars: TFullParanPosArray; PHouseCusps: THouseCusps);
    property SolSysPoints: TFullParanPosArray read FSolSysPoints;
    property FixStars: TFullParanPosArray read FFixstars;
    property HouseCusps: THouseCusps read FHouseCusps;
  end;


  { TParansPosRequest }

  TParansPosRequest = class
  private
    FFixStarSet: TEnumFixStarSets;
    FSolSysPointSet: TEnumSolSysPointSets;
    FJulian: double;
    FLocation: TLocation;
    FHouseSystem: string;
    FSettings: TSettings;
  public
    constructor Create(PFixStarSet: TEnumFixStarSets; PSolSysPointSet: TEnumSolSysPointSets;
      PJulian: double; PLocation: TLocation; PHouseSystem: string; PSettings: TSettings);
    property FixStarSet: TEnumFixStarSets read FFixStarSet;
    property SolSysPointSet: TEnumSolSysPointSets read FSolSysPointSet;
    property Julian: double read FJulian;
    property Location: TLocation read FLocation;
    property HouseSystem: string read FHouseSystem;
    property Settings: TSettings read FSettings;
  end;

  { TParansPosResponse }

  TParansPosResponse = class
  private
    FParanCalculationResult: TParanCalculationResult;
    FErrorTxt: string;
  public
    constructor Create(PParanCalculationResult: TParanCalculationResult; PErrorTxt: string);
    property ParanCalculationResult: TParanCalculationResult read FParanCalculationResult;
    property ErrorTxt: string read FErrorTxt;
  end;

  { TParansMatchesRequest }

  TParansMatchesRequest = class
  strict private
    FSolSysPoints, FFixStars: TFullParanPosArray;
    FOrbis: double;
  public
    constructor Create(PSolSysPoints, PFixStars: TFullParanPosArray; POrbis: double);
    property SolSysPoints: TFullParanPosArray read FSolSysPoints;
    property FixStars: TFullParanPosArray read FFixStars;
    property Orbis: double read FOrbis;
  end;

  { TParansMatchesResponse }

  TParansMatchesResponse = class
  strict private
    FParanMatches: TParanMatchesArray;
    FErrorTxt: string;
  public
    constructor Create(PParanMatches: TParanMatchesArray; PErrorTxt: string);
    property ParanMatches: TParanMatchesArray read FParanMatches;
    property ErrorTxt: string read FErrorTxt;
  end;


  { TChartData }
  { Results from data input. }
  TChartData = class
  strict private
    FNameDescr: string;
    FLocation: TLocation;
    FJulianDay: double;
  public
    constructor Create(PNameDescr: string; PLongitude, PLatitude, PAltitude, PJulianDay: double);
    property Namedescr: string read FNameDescr;
    property Location: TLocation read FLocation;
    property JulianDay: double read FJulianDay;
  end;


implementation

uses
  UnitAstron;

const
  IniFileName = '.\paransettings.ini';

{ TInputValues }

constructor TInputValues.Create(PTimezoneOffset: double; PDst, PGregCal: boolean);
begin
  FTimezoneOffset := PTimezoneOffset;
  FDst := PDst;
  FGregCal := PGregCal;
end;

{ TSettings }

procedure TSettings.ReadFromIni;
var
  IniFile: TIniFile;
  Section: string;
begin
  Section := 'settings';
  IniFile := TIniFile.Create(IniFileName);
  FModernPlanets := IniFile.ReadBool(Section, 'modernplanets', True);
  FOrbSeconds := IniFile.ReadInteger(Section, 'orbseconds', 120);
  IniFile.Free;
end;


constructor TSettings.Create(PModernPlanets: boolean; POrbSeconds: integer);
begin
  FModernPlanets := PModernPlanets;
  FOrbSeconds := POrbSeconds;
end;

constructor TSettings.Create;
begin
  ReadFromIni;
end;

procedure TSettings.Persist;
var
  IniFile: TIniFile;
  Section: string;
begin
  Section := 'settings';
  IniFile := TIniFile.Create(IniFileName);
  IniFile.WriteBool(Section, 'modernplanets', FModernPlanets);
  IniFile.WriteInteger(Section, 'orbseconds', FOrbSeconds);
  IniFile.Free;
end;

{ TParansMatchesResponse }

constructor TParansMatchesResponse.Create(PParanMatches: TParanMatchesArray; PErrorTxt: string);
begin
  FParanMatches := PParanMatches;
  FErrorTxt := PErrorTxt;
end;

{ TParansMatchesRequest }

constructor TParansMatchesRequest.Create(PSolSysPoints, PFixStars: TFullParanPosArray; POrbis: double);
begin
  FSolSysPoints := PSolSysPoints;
  FFixStars := PFixStars;
  FOrbis := POrbis;
end;

{ TParanMatches }

constructor TParanMatches.Create(PBase: TParanData; PMatches: TParanDataArray);
begin
  FBase := PBase;
  FMatches := PMatches;
end;

{ TParanData }

constructor TParanData.Create(PCelPointSpec: TCelPointSpec; PJulianDay: double; PParanPos: TEnumParanPositions);
begin
  FCelPointSpec := PCelPointSpec;
  FJulianDay := PJulianDay;
  FParanPos := PParanPos;
end;

{ TParanCalculationResult }

constructor TParanCalculationResult.Create(PSolSysPoints, PFixStars: TFullParanPosArray; PHouseCusps: THouseCusps);
begin
  FSolSysPoints := PSolSysPoints;
  FFixStars := PFixStars;
  FHouseCusps := PHouseCusps;
end;

{ TFullParanPos }

constructor TFullParanPos.Create(PCelPointSpec: TCelPointSpec;
  PFullEclPos, PEquatorialPos, PHorizontalPos, PParanTimes: TDblArray);
begin
  FCelPointSpec := PCelPointSpec;
  FFullEclPos := PFullEclPos;
  FEquatorialPos := PEquatorialPos;
  FHorizontalPos := PHorizontalPos;
  FParanTimes := PParanTimes;
end;

{ THouseCusps }

constructor THouseCusps.Create(PMc, PAsc, PMcDecl, PAscDecl: double; PCusps, PCuspsDecl: TDblArray);
begin
  FMc := PMc;
  FAsc := PAsc;
  FMcDecl := PMcDecl;
  FAscDecl := PAscDecl;
  FCusps := PCusps;
  FCuspsDecl := PCuspsDecl;
end;

{ TTimeZoneSpec }

constructor TTimeZoneSpec.Create(PName: string; POffset: double);
begin
  FOffset := POffset;
  FName := PName;
end;

{ TSolSysPointSpec }

constructor TSolSysPointSpec.Create(PId, PSeId: integer; PName: string);
begin
  FSeId := PSeId;
  inherited Create(PId, PName);
end;

{ TFixStarSpec }

constructor TFixStarSpec.Create(PId: integer; PName, PFormalName, PSearchArg: string);
begin
  FFormalName := PFormalName;
  FSearchArg := PSearchArg;
  inherited Create(PId, PName);
end;

{ TCelPointSpec }

constructor TCelPointSpec.Create(PId: integer; PName: string);
begin
  Fid := PId;
  FName := PName;
end;

{ TFullDatetime }

procedure TFullDatetime.ConstructFormattedTexts;  // yyyyy/mm/dd hh:mi:ss in local time
var
  Fmt2, Fmt5, YearTxt, MonthTxt, DayTxt, HourTxt, MinuteTxt, SecondTxt: string;
begin
  Fmt2 := '%2.2d';
  Fmt5 := '%5.4d';
  YearTxt := Format(Fmt5, [FYear]);
  MonthTxt := Format(Fmt2, [FMonth]);
  DayTxt := Format(Fmt2, [FDay]);
  HourTxt := Format(Fmt2, [FHour]);
  MinuteTxt := Format(Fmt2, [FMinute]);
  SecondTxt := Format(Fmt2, [FSecond]);
  FFormattedText := YearTxt + '/' + MonthTxt + '/' + DayTxt + ' ' + HourTxt + ':' + MinuteTxt + ':' + SecondTxt;
  FFormattedMDTime := MonthTxt + '/' + DayTxt + ' ' + HourTxt + ':' + MinuteTxt + ':' + SecondTxt;
end;

procedure TFullDatetime.DefineUt;
begin
  FClockTime := FHour + FMinute / 60.0 + FSecond / 3600.0;
end;

constructor TFullDatetime.Create(PYear, PMonth, PDay, PHour, PMinute, PSecond: integer;
  PTzOffset: double; PDst, PGregCal: boolean);
begin
  FYear := PYear;
  FMonth := PMonth;
  FDay := PDay;
  FHour := PHour;
  FMinute := PMinute;
  FSecond := PSecond;
  FTzOffset := PTzOffset;
  FDst := PDst;
  FGregCal := PGregCal;
  ConstructFormattedTexts;
  DefineUt;
end;

constructor TFullDatetime.Create(PJulianDay, PTzOffset: double; PDst, PGregCal: boolean);
var
  Ephemeris: TEphemeris;
  DateTime: TDateTime;
  JdCorrectedForClock, FracHour, FracMinute: double;
begin
  Ephemeris := TEphemeris.Create;
  JdCorrectedForClock := PJulianDay + PTzOffset / 24.0;
  if (PDst) then JdCorrectedForClock := JdCorrectedForClock + 1.0 / 24.0;
  DateTime := Ephemeris.ConstructDateTime(JdCorrectedForClock, PGregCal);
  FreeAndNil(Ephemeris);
  FYear := DateTime.Year;
  FMonth := DateTime.Month;
  FDay := DateTime.Day;
  FHour := Trunc(DateTime.Ut);
  FracHour := Frac(DateTime.Ut);
  FMinute := Trunc(FracHour * 60.0);
  FracMinute := Frac(FracHour * 60.0);
  FSecond := Trunc(FracMinute * 60.0);
  FTzOffset := PTzOffset;
  FDst := PDst;
  FGregCal := PGregCal;
  ConstructFormattedTexts;
  DefineUt;
end;



{ TParansPosResponse }

constructor TParansPosResponse.Create(PParanCalculationResult: TParanCalculationResult; PErrorTxt: string);
begin
  FParanCalculationResult := PParanCalculationResult;
  FErrorTxt := PErrorTxt;
end;

{ TParansPosRequest }

constructor TParansPosRequest.Create(PFixStarSet: TEnumFixStarSets; PSolSysPointSet: TEnumSolSysPointSets;
  PJulian: double; PLocation: TLocation; PHouseSystem: string; PSettings: TSettings);
begin
  FFixStarSet := PFixStarSet;
  FSolSysPointSet := PSolSysPointSet;
  FJulian := PJulian;
  FLocation := PLocation;
  FHouseSystem := PHouseSystem;
  FSettings := PSettings;
end;

{ TChartData }

constructor TChartData.Create(PNameDescr: string; PLongitude, PLatitude, PAltitude, PJulianDay: double);
begin
  FNameDescr := PNameDescr;
  FLocation.Longitude := PLongitude;
  FLocation.Latitude := PLatitude;
  FLocation.Altitude := PAltitude;
  FJulianDay := PJulianDay;
end;

end.
