{ Jan Kampherbeek, (c)  2021.
  Enigma Parans is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit unithandlers;

{< Handlers, called from an API, process the incoming request.}

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, unitanalysis, UnitAstron, UnitDomainXchg, UnitReferences;

type

  { TAllParansHandler }

  TAllParansHandler = class
  strict private
    ParanRefs: TParanRefs;
    Ephemeris: TEphemeris;
    function CreateFixStarSpecs: TFixStarSpecArray;
    function CreateSolSysPointSpecs(SolSysPOintSet: TEnumSolSysPointSets): TSolSysPointSpecArray;
    function CreateFullParanSet(PFixStars: TFixStarSpecArray; PSolSysPoints: TSolSysPointSpecArray;
      PLocation: TLocation; PJulianDay, PObliquity: double; PHouseSystem: string): TParanCalculationResult;
  public
    constructor Create;
    destructor Destroy; override;
    function Process(PRequest: TParansPosRequest): TParansPosResponse;
  end;

  { TParanMatchesHandler }

  TParanMatchesHandler = class
  strict private
    ParanMatcher: TParanMatcher;
  public
    constructor Create;
    destructor Destroy; override;
    function Process(Request: TParansMatchesRequest): TParansMatchesResponse;
  end;

  { TJulianDayHandler }

  TJulianDayHandler = class
  strict private
    Ephemeris: TEphemeris;
  public
    constructor Create;
    destructor Destroy; override;
    function CheckAndHandleDate(PYear, PMonth, PDay: integer; PGregorian: boolean): TValidatedDouble;
    function FullDateTimeFromJd(PJulianDay, PTzOffset: double; PDst, PGregorian: boolean): TFullDateTime;
  end;


implementation

{ TParanMatchesHandler }

constructor TParanMatchesHandler.Create;
begin
  ParanMatcher := TParanMatcher.Create;
end;

destructor TParanMatchesHandler.Destroy;
begin
  FreeAndNil(ParanMatcher);
  inherited Destroy;
end;

function TParanMatchesHandler.Process(Request: TParansMatchesRequest): TParansMatchesResponse;
var
  Matches: TParanMatchesArray;
  ErrorTxt: string;
begin
  Matches := ParanMatcher.FindMatches(Request.SolSysPoints, Request.FixStars, Request.Orbis);
  ErrorTxt := '';
  Result := TParansMatchesResponse.Create(Matches, ErrorTxt);
end;

{ TJulianDayHandler }

constructor TJulianDayHandler.Create;
begin
  Ephemeris := TEphemeris.Create;
end;

destructor TJulianDayHandler.Destroy;
begin
  FreeAndNil(Ephemeris);
  inherited Destroy;
end;

function TJulianDayHandler.CheckAndHandleDate(PYear, PMonth, PDay: integer; PGregorian: boolean): TValidatedDouble;
begin
  Result := Ephemeris.CheckDate(PYear, PMonth, PDay, PGregorian);
end;

function TJulianDayHandler.FullDateTimeFromJd(PJulianDay, PTzOffset: double; PDst, PGregorian: boolean): TFullDateTime;
begin
  Result := Ephemeris.ConstructFullDateTime(PJulianDay, PTzOffset, PDst, PGregorian);
end;


{ TAllParansHandler }

constructor TAllParansHandler.Create;
begin
  Ephemeris := TEphemeris.Create;
  ParanRefs := TParanRefs.Create;
end;

destructor TAllParansHandler.Destroy;
begin
  FreeAndNil(ParanRefs);
  FreeAndNil(Ephemeris);
  inherited Destroy;
end;

function TAllParansHandler.CreateFixStarSpecs: TFixStarSpecArray;
var
  i: integer;
  AllFixStars, SelectedFixStars: TFixStarSpecArray;
begin
  AllFixStars := ParanRefs.AllFixStarSpecs;
  SelectedFixStars := [];
  // In version 1.0 the FixStarSet is always Ptolemy.
  SetLength(SelectedFixStars, 29);
  for i := 0 to 28 do SelectedFixStars[i] := AllFixStars[i];
  Result := SelectedFixStars;
end;

function TAllParansHandler.CreateSolSysPointSpecs(SolSysPOintSet: TEnumSolSysPointSets): TSolSysPointSpecArray;
var
  i: integer;
  AllSolSysPoints, SelectedSolSysPoints: TSolSysPointSpecArray;
begin
  AllSolSysPoints := ParanRefs.AllSolSysPoints;
  SelectedSolSysPoints := [];
  if (SolSysPointSet = classical) then begin
    SetLength(SelectedSolSysPoints, 7);
    for i := 0 to 6 do SelectedSolSysPoints[i] := AllSolSysPoints[i];
  end else begin
    SetLength(SelectedSolSysPoints, 10);
    for i := 0 to 10 do SelectedSolSysPoints[i] := AllSolSysPoints[i];
  end;
  Result := SelectedSolSysPoints;
end;

function TAllParansHandler.CreateFullParanSet(PFixStars: TFixStarSpecArray;
  PSolSysPoints: TSolSysPointSpecArray; PLocation: TLocation; PJulianDay, PObliquity: double;
  PHouseSystem: string): TParanCalculationResult;
var
  FixStarResults, SolSysPointResults: TFullParanPosArray;
  HouseResults: THouseCusps;
  ShiftedJulianDay: double;
  i: integer;
  CalcResult: TParanCalculationResult;
begin
  FixStarResults := [];
  SolSysPointResults := [];
  ShiftedJulianDay := Ephemeris.ShiftedJulianDay(PJulianDay, PLocation);
  SetLength(FixStarResults, Length(PFixStars));
  for i := 0 to Length(PFixStars) - 1 do FixStarResults[i] :=
      Ephemeris.PositionsForFixStar(PFixStars[i], PJulianDay, ShiftedJulianDay, PObliquity, PLocation);
  SetLength(SolSysPointResults, Length(PSolSysPoints));
  for i := 0 to Length(PSolSysPoints) - 1 do SolSysPointResults[i] :=
      Ephemeris.PositionsForSolSysPoint(PSolSysPoints[i], PJulianDay, ShiftedJulianDay, PObliquity, PLocation);
  HouseResults := Ephemeris.PositionsHouses(PHouseSystem, PJulianDay, PLocation);
  CalcResult := TParanCalculationResult.Create(SolSysPointResults, FixStarResults, HouseResults);
  Result := CalcResult;
end;

function TAllParansHandler.Process(PRequest: TParansPosRequest): TParansPosResponse;
var
  SelectedFixStars: TFixStarSpecArray;
  SelectedSolSysPoints: TSolSysPointSpecArray;
  Obliquity: double;
  CalculationResult: TParanCalculationResult;
  Error: string;
begin
  SelectedFixStars := CreateFixStarSpecs;
  SelectedSolSysPoints := CreateSolSysPointSpecs(PRequest.SolSysPointSet);
  Obliquity := Ephemeris.Obliquity(PRequest.Julian);
  CalculationResult := CreateFullParanset(SelectedFixStars, SelectedSolSysPoints, PRequest.Location,
    PRequest.Julian, Obliquity, PRequest.HouseSystem);
  Error := '';
  Result := TParansPosResponse.Create(CalculationResult, Error);
end;

end.





