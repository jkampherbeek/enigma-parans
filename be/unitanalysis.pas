{ Jan Kampherbeek, (c)  2021.
  Enigma Parans is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit unitanalysis;

{< Analysis functionality in the backend.}

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, UnitDomainXchg;

type

  { TParanMatcher }

  TParanMatcher = class
  private
    SolSysPoints, FixStars: TFullParanPosArray;
    function CheckAllPairs(PCurrentIndex, PParanPosIndex: integer; PSolSysPoint: TFullParanPos;
      POrbis: double): TParanDataArray;
    function ProcessParans(PSolSysPoints, PFixStars: TFullParanPosArray; POrbis: double): TParanMatchesArray;
  public
    // Orbis in time, measure as fraction of a day.
    function FindMatches(PSolSysPoints, PFixStars: TFullParanPosArray; POrbis: double): TParanMatchesArray;
  end;

implementation

{ TParanMatcher }


function TParanMatcher.CheckAllPairs(PCurrentIndex, PParanPosIndex: integer; PSolSysPoint: TFullParanPos;
  POrbis: double): TParanDataArray;
var
  i, j, Count: integer;
  BaseTime, Candidatetime: double;
  ParanDataArray: TParanDataArray;
begin
  ParanDataArray := [];
  Count := 0;
  BaseTime := PSolSysPoint.ParanTimes[PParanPosIndex];
  for i := PCurrentIndex + 1 to Length(SolSysPoints) - 1 do for j := 0 to 3 do begin
      CandidateTime := SolSysPoints[i].ParanTimes[j];
      if (Abs(BaseTime - CandidateTime) < POrbis) then begin
        Inc(Count);
        SetLength(ParanDataArray, Count);
        ParanDataArray[Count - 1] := TParanData.Create(SolSysPoints[i].CelPointSpec, CandidateTime,
          TEnumParanPositions(j));
      end;
    end;
  for i := 0 to Length(FixStars) - 1 do for j := 0 to 3 do begin
      CandidateTime := FixStars[i].ParanTimes[j];
      if (Abs(BaseTime - CandidateTime) < POrbis) then begin
        Inc(Count);
        SetLength(ParanDataArray, Count);
        ParanDataArray[Count - 1] := TParanData.Create(FixStars[i].CelPointSpec, CandidateTime, TEnumParanPositions(j));
      end;
    end;
  Result := ParanDataArray;
end;

function TParanMatcher.ProcessParans(PSolSysPoints, PFixStars: TFullParanPosArray; POrbis: double): TParanMatchesArray;
var
  i, j, Count: integer;
  Base: TParanData;
  ParanMatchesArray: TParanMatchesArray = ();
  ParanDataMatches: TParanDataArray;
  SolSysPoint: TFullParanPos;
  ParanPos: TEnumParanPositions;
begin
  SolSysPoints := PSolSysPoints;
  FixStars := PFixStars;
  Count := 0;
  for i := 0 to 3 do begin
    ParanPos := TEnumParanPositions(i);
    for j := 0 to Length(PSolSysPoints) - 1 do begin
      SolSysPoint := PSolSysPoints[j];
      ParanDataMatches := CheckAllPairs(j, i, SolSysPoint, POrbis);
      if (Length(ParanDataMatches) > 0) then begin
        Inc(Count);
        SetLength(ParanMatchesArray, Count);
        Base := TParanData.Create(SolSysPoint.CelPointSpec, SolSysPoint.ParanTimes[i], ParanPos);
        ParanMatchesArray[Count - 1] := TParanMatches.Create(Base, ParanDataMatches);
      end;
    end;
  end;
  Result := ParanMatchesArray;


end;


function TParanMatcher.FindMatches(PSolSysPoints, PFixStars: TFullParanPosArray; POrbis: double): TParanMatchesArray;
begin
  Result := ProcessParans(PSolSysPoints, PFixStars, POrbis);
end;



end.
