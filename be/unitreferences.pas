{ Jan Kampherbeek, (c)  2021.
  Enigma Parans is open source.
  Please check the file copyright.txt in the root of the source for further details }

unit UnitReferences;

{< Values for lookup lists and other references.}

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, UnitDomainXchg;

type

  { TParanRefs }

  TParanRefs = class
  private
    FAllFixStarSpecs: TFixStarSpecArray;
    FAllSolSysPointSpecs: TSolSysPointSpecArray;
    FAllTimeZoneSpecs: TTimeZoneSpecArray;
    FAllLatitudeDirections: TGeoDirSpecArray;
    FAllLongitudeDirections: TGeoDirSpecArray;
    FAllCalendars: TCalendarSpecArray;
    FAllYearCounts: TYearCountSpecArray;
    procedure CreateFixStarsSpec;
    procedure CreateSolSysPointsSpec;
    procedure CreateTimeZonesSpec;
    procedure CreateGeoDirectionsSpec;
    procedure CreateCalendarsSpec;
    procedure CreateYearCountsSpec;
    function CreateSingleMonth(PIndex: integer; PName: string): TMonthSpec;
    function CreateSingleGeoDir(PName: string; PDir: integer): TGeoDirSpec;
    function CreateSingleCalendar(PIndex: integer; PName: string): TCalendarSpec;
    function CreateSingleYearCount(PIndex: integer; PName: string): TYearCountSpec;
  public
    constructor Create;
    property AllFixStarSpecs: TFixStarSpecArray read FAllFixStarSpecs;
    property AllSolSysPoints: TSolSysPointSpecArray read FAllSolSysPointSpecs;
    property AllTimeZones: TTimeZoneSpecArray read FAllTimeZoneSpecs;
    property AllLatitudeDirections: TGeoDirSpecArray read FAllLatitudeDirections;
    property AllLongitudeDirections: TGeoDirSpecArray read FAllLongitudeDirections;
    property AllCalendars: TCalendarSpecArray read FAllCalendars;
    property AllYearCounts: TYearCountSpecArray read FAllYearCounts;
  end;

implementation

uses
  swissdelphi;

{ TParanRefs }

procedure TParanRefs.CreateFixStarsSpec;
begin
  SetLength(FAllFixStarSpecs, 29);
  FAllFixStarSpecs[0] := TFixStarSpec.Create(0, 'Vega', 'Alpha Lyrae', 'Vega         ,alLyr');
  FAllFixStarSpecs[1] := TFixStarSpec.Create(1, 'Arcturus', 'Alpha Bootes', 'Arcturus     ,alBoo');
  FAllFixStarSpecs[2] := TFixStarSpec.Create(2, 'Regulus', 'Alpha Leonis', 'Regulus    ,alLeo');
  FAllFixStarSpecs[3] := TFixStarSpec.Create(3, 'Denobola', 'Beta Leonis', 'Denebola     ,beLeo');
  FAllFixStarSpecs[4] := TFixStarSpec.Create(4, 'Aldebaran', 'Alpha Tauri', 'Aldebaran  ,alTau');
  FAllFixStarSpecs[5] := TFixStarSpec.Create(5, 'Procyon', 'Alpha Canis Minoris', 'Procyon      ,alCMi');
  FAllFixStarSpecs[6] := TFixStarSpec.Create(6, 'Betelgeuse', 'Alpha Orionis', 'Betelgeuse   ,alOri');
  FAllFixStarSpecs[7] := TFixStarSpec.Create(7, 'Spica', 'Alpha Virginis', 'Spica      ,alVir');
  FAllFixStarSpecs[8] := TFixStarSpec.Create(8, 'Rigel', 'Beta Orionis', 'Rigel        ,beOri');
  FAllFixStarSpecs[9] := TFixStarSpec.Create(9, 'Sirius', 'Alpha Canis Maioris', 'Sirius     ,alCMa');
  FAllFixStarSpecs[10] := TFixStarSpec.Create(10, 'Fomalhaut', 'Alpha Piscis Australis', 'Fomalhaut    ,alPsA');
  FAllFixStarSpecs[11] := TFixStarSpec.Create(11, 'Achernar', 'Alpha Eridani', 'Achernar     ,alEri');
  FAllFixStarSpecs[12] := TFixStarSpec.Create(12, 'Canopus', 'Alpha Carinae', 'Canopus      ,alCar');
  FAllFixStarSpecs[13] := TFixStarSpec.Create(13, 'Algol', 'Beta Persei', 'Algol      ,bePer');
  FAllFixStarSpecs[14] := TFixStarSpec.Create(14, 'Menkalinan', 'Beta Aurigae', 'Menkalinan   ,beAur');
  FAllFixStarSpecs[15] := TFixStarSpec.Create(15, 'Deneb Adiga', 'Alpha Cygni', 'Deneb Adige ,alCyg');
  FAllFixStarSpecs[16] := TFixStarSpec.Create(16, 'Alphecca', 'Alpha Coronae Borealis', 'Alphecca     ,alCrB');
  FAllFixStarSpecs[17] := TFixStarSpec.Create(17, 'Castor', 'Alpha Geminorum', 'Castor       ,alGem');
  FAllFixStarSpecs[18] := TFixStarSpec.Create(18, 'Pollux', 'Beta Geminorum', 'Pollux       ,beGem');
  FAllFixStarSpecs[19] := TFixStarSpec.Create(19, 'Alpheratz', 'Alpha Andromedae', 'Alpheratz    ,alAnd');
  FAllFixStarSpecs[20] := TFixStarSpec.Create(20, 'Altair', 'Alpha Aquilae', 'Altair       ,alAql');
  FAllFixStarSpecs[21] := TFixStarSpec.Create(21, 'Bellatrix', 'Gamma Orionis', 'Bellatrix    ,gaOri');
  FAllFixStarSpecs[22] := TFixStarSpec.Create(22, 'Alphard', 'Alpha Hydrae', 'Alphard      ,alHya');
  FAllFixStarSpecs[23] := TFixStarSpec.Create(23, 'Zubeneshamali', 'Beta Librae', 'Zubeneshamali,beLib');
  FAllFixStarSpecs[24] := TFixStarSpec.Create(24, 'Alnilam', 'Epsilon Orionis', 'Alnilam      ,epOri');
  FAllFixStarSpecs[25] := TFixStarSpec.Create(25, 'Antares', 'Alpha Scorpii', 'Antares    ,alSco');
  FAllFixStarSpecs[26] := TFixStarSpec.Create(26, 'Rukbat', 'Alpha Sagittarii', 'Rukbat       ,alSgr');
  FAllFixStarSpecs[27] := TFixStarSpec.Create(27, 'Capella', 'Alpha Aurigae', 'Capella      ,alAur');
  FAllFixStarSpecs[28] := TFixStarSpec.Create(28, 'Zubenelgenubi', 'Alpha Librae', 'Zubenelgenubi,al-2Lib');
end;

procedure TParanRefs.CreateSolSysPointsSpec;
begin
  SetLength(FAllSolSysPointSpecs, 10);
  FAllSolSysPointSpecs[0] := TSolSysPointSpec.Create(0, SE_SUN, 'Sun');
  FAllSolSysPointSpecs[1] := TSolSysPointSpec.Create(1, SE_MOON, 'Moon');
  FAllSolSysPointSpecs[2] := TSolSysPointSpec.Create(2, SE_MERCURY, 'Mercury');
  FAllSolSysPointSpecs[3] := TSolSysPointSpec.Create(3, SE_VENUS, 'Venus');
  FAllSolSysPointSpecs[4] := TSolSysPointSpec.Create(4, SE_MARS, 'Mars');
  FAllSolSysPointSpecs[5] := TSolSysPointSpec.Create(5, SE_JUPITER, 'Jupiter');
  FAllSolSysPointSpecs[6] := TSolSysPointSpec.Create(6, SE_SATURN, 'Saturn');
  FAllSolSysPointSpecs[7] := TSolSysPointSpec.Create(7, SE_URANUS, 'Uranus');
  FAllSolSysPointSpecs[8] := TSolSysPointSpec.Create(8, SE_NEPTUNE, 'Neptune');
  FAllSolSysPointSpecs[9] := TSolSysPointSpec.Create(9, SE_PLUTO, 'Pluto');
end;

procedure TParanRefs.CreateTimeZonesSpec;
begin
  SetLength(FAllTimeZoneSpecs, 33);
  FAllTimeZoneSpecs[0] := TTimeZoneSpec.Create('tzut', 0.0);
  FAllTimeZoneSpecs[1] := TTimeZoneSpec.Create('tzcet', 1.0);
  FAllTimeZoneSpecs[2] := TTimeZoneSpec.Create('tzeet', 2.0);
  FAllTimeZoneSpecs[3] := TTimeZoneSpec.Create('tzeat', 3.0);
  FAllTimeZoneSpecs[4] := TTimeZoneSpec.Create('tzirst', 3.5);
  FAllTimeZoneSpecs[5] := TTimeZoneSpec.Create('tzamt', 4.0);
  FAllTimeZoneSpecs[6] := TTimeZoneSpec.Create('tzaft', 4.5);
  FAllTimeZoneSpecs[7] := TTimeZoneSpec.Create('tzpkt', 5.0);
  FAllTimeZoneSpecs[8] := TTimeZoneSpec.Create('tzist', 5.5);
  FAllTimeZoneSpecs[9] := TTimeZoneSpec.Create('tziot', 6.0);
  FAllTimeZoneSpecs[10] := TTimeZoneSpec.Create('tzmmt', 6.5);
  FAllTimeZoneSpecs[11] := TTimeZoneSpec.Create('tzict', 7.0);
  FAllTimeZoneSpecs[12] := TTimeZoneSpec.Create('tzwst', 8.0);
  FAllTimeZoneSpecs[13] := TTimeZoneSpec.Create('tzjst', 9.0);
  FAllTimeZoneSpecs[14] := TTimeZoneSpec.Create('tzacst', 9.5);
  FAllTimeZoneSpecs[15] := TTimeZoneSpec.Create('tzaest', 10.0);
  FAllTimeZoneSpecs[16] := TTimeZoneSpec.Create('tzlhst', 10.5);
  FAllTimeZoneSpecs[17] := TTimeZoneSpec.Create('tznct', 11.0);
  FAllTimeZoneSpecs[18] := TTimeZoneSpec.Create('tznzst', 12.0);
  FAllTimeZoneSpecs[19] := TTimeZoneSpec.Create('tzsst', -11.0);
  FAllTimeZoneSpecs[20] := TTimeZoneSpec.Create('tzhast', -10.0);
  FAllTimeZoneSpecs[21] := TTimeZoneSpec.Create('tzmart', -9.5);
  FAllTimeZoneSpecs[22] := TTimeZoneSpec.Create('tzakst', -9.0);
  FAllTimeZoneSpecs[23] := TTimeZoneSpec.Create('tzpst', -8.0);
  FAllTimeZoneSpecs[24] := TTimeZoneSpec.Create('tzmst', -7.0);
  FAllTimeZoneSpecs[25] := TTimeZoneSpec.Create('tzcst', -6.0);
  FAllTimeZoneSpecs[26] := TTimeZoneSpec.Create('tzest', -5.0);
  FAllTimeZoneSpecs[27] := TTimeZoneSpec.Create('tzast', -4.0);
  FAllTimeZoneSpecs[28] := TTimeZoneSpec.Create('tznst', -3.5);
  FAllTimeZoneSpecs[29] := TTimeZoneSpec.Create('tzbrt', -3.0);
  FAllTimeZoneSpecs[30] := TTimeZoneSpec.Create('tzgst', -2.0);
  FAllTimeZoneSpecs[31] := TTimeZoneSpec.Create('tzazot', -1.0);
  FAllTimeZoneSpecs[32] := TTimeZoneSpec.Create('tzlmt', 0.0);
end;

procedure TParanRefs.CreateGeoDirectionsSpec;
begin
  SetLength(FAllLatitudeDirections, 2);
  SetLength(FAllLongitudeDirections, 2);
  FAllLatitudeDirections[0] := CreateSingleGeoDir('geolatnorth', 1);
  FAllLatitudeDirections[1] := CreateSingleGeoDir('geolatsouth', -1);
  FAllLongitudeDirections[0] := CreateSingleGeoDir('geolongeast', 1);
  FAllLongitudeDirections[1] := CreateSingleGeoDir('geolongwest', -1);
end;

procedure TParanRefs.CreateCalendarsSpec;
begin
  SetLength(FAllCalendars, 2);
  FAllCalendars[0] := CreateSingleCalendar(0, 'gregorian');
  FAllCalendars[1] := CreateSingleCalendar(1, 'julian');
end;

procedure TParanRefs.CreateYearCountsSpec;
begin
  SetLength(FAllYearCounts, 3);
  FAllYearCounts[0] := CreateSingleYearCount(0, 'yearcountce');
  FAllYearCounts[1] := CreateSingleYearCount(0, 'yearcountbce');
  FAllYearCounts[2] := CreateSingleYearCount(0, 'yearcountastron');
end;

function TParanRefs.CreateSingleMonth(PIndex: integer; PName: string): TMonthSpec;
var
  NewSpec: TMonthSpec;
begin
  NewSpec.Index := PIndex;
  NewSpec.Name := PName;
  Result := NewSpec;
end;

function TParanRefs.CreateSingleGeoDir(PName: string; PDir: integer): TGeoDirSpec;
var
  NewSpec: TGeoDirSpec;
begin
  NewSpec.Name := PName;
  NewSpec.PlusMinus := PDir;
  Result := NewSpec;
end;

function TParanRefs.CreateSingleCalendar(PIndex: integer; PName: string): TCalendarSpec;
var
  NewSpec: TCalendarSpec;
begin
  NewSpec.Name := PName;
  NewSpec.Index := PIndex;
  Result := NewSpec;
end;


function TParanRefs.CreateSingleYearCount(PIndex: integer; PName: string): TYearCountSpec;
var
  NewSpec: TYearCountSpec;
begin
  NewSpec.Name := PName;
  NewSpec.Index := PIndex;
  Result := NewSpec;
end;

constructor TParanRefs.Create;
begin
  CreateFixStarsSpec;
  CreateSolSysPointsSpec;
  CreateTimeZonesSpec;
  CreateGeoDirectionsSpec;
  CreateCalendarsSpec;
  CreateYearCountsSpec;
end;

end.


