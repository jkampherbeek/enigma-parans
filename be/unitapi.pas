{ Jan Kampherbeek, (c)  2021.
  Enigma Parans is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit UnitAPI;

{< API, requests and responses.}

{$mode objfpc}{$H+}


interface

uses
  Classes, SysUtils, UnitDomainXchg, UnitHandlers;

type

  { TAllParansApi }

  TAllParansApi = class
  private
    Handler: TAllParansHandler;
  public
    constructor Create;
    function GetParans(PRequest: TParansPosRequest): TParansPosResponse;
  end;

  { TParanMatchesApi }

  TParanMatchesApi = class
  strict private
    Handler: TParanMatchesHandler;
  public
    constructor Create;
    destructor Destroy; override;
    function FindMatches(PRequest: TParansMatchesRequest): TParansMatchesResponse;
  end;

  { TJulianDayApi }

  TJulianDayApi = class
  private
    Handler: TJulianDayHandler;
  public
    constructor Create;
    destructor Destroy; override;
    function CheckAndHandleDate(PYear, PMonth, PDay: integer; PGregorian: boolean): TValidatedDouble;
    function FullDateTimeFromJd(PJulianDay, PTzOffset: double; PDst, PGregCal: boolean): TFullDatetime;
  end;


implementation

{ TParanMatchesApi }

constructor TParanMatchesApi.Create;
begin
  Handler := TParanMatchesHandler.Create;
end;

destructor TParanMatchesApi.Destroy;
begin
  FreeAndNil(Handler);
  inherited Destroy;
end;

function TParanMatchesApi.FindMatches(PRequest: TParansMatchesRequest): TParansMatchesResponse;
begin
  Result := Handler.Process(PRequest);
end;

{ TJulianDayApi }

constructor TJulianDayApi.Create;
begin
  Handler := TJulianDayHandler.Create;
end;

destructor TJulianDayApi.Destroy;
begin
  FreeAndNil(Handler);
  inherited Destroy;
end;

function TJulianDayApi.CheckAndHandleDate(PYear, PMonth, PDay: integer; PGregorian: boolean): TValidatedDouble;
begin
  Result := Handler.CheckAndHandleDate(PYear, PMonth, PDay, PGregorian);
end;

function TJulianDayApi.FullDateTimeFromJd(PJulianDay, PTzOffset: double; PDst, PGregCal: boolean): TFullDatetime;
begin
  Result := Handler.FullDateTimeFromJd(PJulianDay, PTzOffset, PDst, PGregCal);
end;

{ TAllParansApi }

constructor TAllParansApi.Create;
begin
  Handler := TAllParansHandler.Create;
end;

function TAllParansApi.GetParans(PRequest: TParansPosRequest): TParansPosResponse;
begin
  Result := Handler.Process(PRequest);
end;


end.



