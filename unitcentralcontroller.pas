{ Jan Kampherbeek, (c)  2021.
  Enigma Parans is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit unitcentralcontroller;

{< Central data handling for frontend. }

{$mode objfpc}{$H+}
{$WARN 3018 off : Constructor should be public}
interface

uses
  Classes, SysUtils, UnitDomainXchg;

type

{ TCenCon }

{ Central Controller. Keeps track of global data. Implemented as singleton. }
TCenCon = class
strict private
  FChartData: TChartData;
  FInputValues: TInputValues;
  FPresInputData: TPresentableInputData;
  FSettings: TSettings;
  FChartDataDefined: boolean;
  constructor Init;
  procedure DefineDefaultSettings;
public
  class function Create: TCenCon;
  procedure SetChartData(PChartData: TChartData);
  procedure SetSettings(PSettings: TSettings);
  procedure SetPresInputData(PPresInputData: TPresentableInputData);
  procedure DefineSettingsFromIni;
  procedure SetInputValues(PInputValues: TInputValues);
  property ChartData: TChartData read FChartData;
  property InputValues: TInputValues read FInputValues;
  property PresInputData: TPresentableInputData read FPresInputData write SetPresInputData;
  property ChartDataDefined: boolean read FChartDataDefined;
  property Settings: TSettings read FSettings write SetSettings;
end;


implementation

var
  CenCon: TCenCon = nil;

{ TCenCon }

constructor TCenCon.Init;
begin
  FChartDataDefined:= false;
  DefineSettingsFromIni;
  inherited Create;
end;

procedure TCenCon.DefineDefaultSettings;
var
  ModernPlanets: boolean;
  OrbSeconds: integer;
begin
  ModernPlanets:= false;
  OrbSeconds:= 120;
  FSettings:= TSettings.Create(ModernPlanets, OrbSeconds);
end;

procedure TCenCon.SetSettings(PSettings: TSettings);
begin
  FSettings:= PSettings;
  FSettings.Persist;
end;

procedure TCenCon.SetPresInputData(PPresInputData: TPresentableInputData);
begin
  FPresInputData:= PPresInputData;
end;

procedure TCenCon.DefineSettingsFromIni;
begin
  FSettings:= TSettings.Create;
end;

procedure TCenCon.SetChartData(PChartData: TChartData);
begin
  FChartData:= PChartData;
  FChartDataDefined:= FChartData <> nil;
end;

procedure TCenCon.SetInputValues(PInputValues: TInputValues);
begin
  FInputValues:= PInputValues;
end;

class function TCenCon.Create: TCenCon;
begin
    if CenCon = nil then CenCon := TCenCon.Init;
  Result:= CenCon;
end;



end.

