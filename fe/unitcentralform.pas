{ Jan Kampherbeek, (c)  2021.
  Enigma Parans is open source.
  Please check the file copyright.txt in the root of the source for further details }

unit Unitcentralform;

{< Main form that shows the calculated results.}

{$mode objfpc}{$H+}

interface

uses
  Classes, ComCtrls,
  Controls, Dialogs, Forms, Graphics, Grids, HelpIntfs, StdCtrls, SysUtils, unitcentralcontroller,
  UnitDomainXchg, unitformatting, unittranslation;

type

  { TFormCentral }

  TFormCentral = class(TForm)
    BtnExit: TButton;
    BtnHelp: TButton;
    BtnNew: TButton;
    LblCbSet: TLabel;
    LblDateTime: TLabel;
    LblNameDescr: TLabel;
    LblLocation: TLabel;
    LblOrb: TLabel;
    LblTitleParans: TLabel;
    LblTitleRadixPos: TLabel;
    PageControl1: TPageControl;
    SGFixStars: TStringGrid;
    SGCelPoints: TStringGrid;
    SgMatches: TStringGrid;
    TabRadixPos: TTabSheet;
    TabParans: TTabSheet;
    procedure BtnExitClick(Sender: TObject);
    procedure BtnHelpClick(Sender: TObject);
    procedure BtnNewClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    CenCon: TCenCon;
    Rosetta: TRosetta;
    ParansRequest: TParansPosRequest;
    ParansResponse: TParansPosResponse;
    ParanMatchesRequest: TParansMatchesRequest;
    ParanMatchesResponse: TParansMatchesResponse;
    SexagPos: TSexagesimalPos;
    ChartData: TChartData;
    procedure PopulateTexts;
    procedure DefineButtons;
    procedure HandleRequests;
    procedure ShowData;
    procedure ShowMatches;
  public

  end;

var
  FormCentral: TFormCentral;

implementation

{$R *.lfm}

uses
  UnitApi, UnitConst, UnitFeSupport;

const
  SECTION = 'formmain';

{ TFormCentral }

procedure TFormCentral.FormCreate(Sender: TObject);
begin
  CenCon := TCencon.Create;
  Rosetta := TRosetta.Create;
  SexagPos := TSexagesimalPos.Create;
end;

procedure TFormCentral.BtnExitClick(Sender: TObject);
begin
  Halt;
end;

procedure TFormCentral.BtnHelpClick(Sender: TObject);
var
  HelpText: string;
begin
  if Rosetta.CurrentLanguage = 'en' then HelpText := 'html/en_centralform.html'
  else
    HelpText := 'html/nl_centralform.html';
  ShowHelpOrErrorForKeyword(EmptyStr, HelpText);
end;

procedure TFormCentral.BtnNewClick(Sender: TObject);
begin
  Close;
end;

procedure TFormCentral.FormShow(Sender: TObject);
begin
  PopulateTexts;
  DefineButtons;
  HandleRequests;
  ShowData;
  ShowMatches;
end;

procedure TFormCentral.PopulateTexts;
var
  OrbInSeconds, Minutes, Seconds: integer;
  CbSetArg: string;
begin
  LblNameDescr.Caption := CenCon.PresInputData.NameDescr;
  if (LblNameDescr.Caption = '') then LblNameDescr.Caption := Rosetta.GetText(SECTION, 'anonymous');
  LblDateTime.Caption := Cencon.PresInputData.DateTime;
  LblLocation.Caption := CenCon.PresInputData.Location;
  OrbInSeconds := Cencon.Settings.OrbSeconds;
  Minutes := OrbInSeconds div 60;
  Seconds := OrbInSeconds - (Minutes * 60);
  if (CenCon.Settings.ModernPlanets) then CbSetArg := 'cbsetmodern'
  else
    CbSetArg := 'cbsetclassic';

  with Rosetta do begin
    Caption := GetText(SECTION, 'formtitle');
    LblTitleRadixPos.Caption := GetText(SECTION, 'titleradixpos');
    TabRadixPos.Caption := GetText(SECTION, 'tabradixpos');
    LblTitleParans.Caption := GetText(SECTION, 'titleparans');
    TabParans.Caption := GetText(SECTION, 'tabparans');
    LblOrb.Caption := GetText(SECTION, 'orbis') + ' ' + IntToStr(Minutes) + ':' + IntToStr(seconds);
    LblCbSet.Caption := GetTExt(SECTION, CbSetArg);

    SGCelPoints.Columns[0].Title.Caption := GetText(LOOKUP_SECTION, 'poslongitude');
    SGCelPoints.Columns[1].Title.Caption := GetText(LOOKUP_SECTION, 'posdeclination');
    SGCelPoints.Columns[2].Title.Caption := GetText(LOOKUP_SECTION, 'posaltitude');
    SGCelPoints.Columns[3].Title.Caption := GetText(LOOKUP_SECTION, 'posazimuth');
    SGCelPoints.Columns[4].Title.Caption := GetText(LOOKUP_SECTION, 'posrise');
    SGCelPoints.Columns[5].Title.Caption := GetText(LOOKUP_SECTION, 'posculm');
    SGCelPoints.Columns[6].Title.Caption := GetText(LOOKUP_SECTION, 'posset');
    SGCelPoints.Columns[7].Title.Caption := GetText(LOOKUP_SECTION, 'posanticulm');

    SGFixStars.Columns[0].Title.Caption := GetText(LOOKUP_SECTION, 'poslongitude');
    SGFixStars.Columns[1].Title.Caption := GetText(LOOKUP_SECTION, 'posdeclination');
    SGFixStars.Columns[2].Title.Caption := GetText(LOOKUP_SECTION, 'posaltitude');
    SGFixStars.Columns[3].Title.Caption := GetText(LOOKUP_SECTION, 'posazimuth');
    SGFixStars.Columns[4].Title.Caption := GetText(LOOKUP_SECTION, 'posrise');
    SGFixStars.Columns[5].Title.Caption := GetText(LOOKUP_SECTION, 'posculm');
    SGFixStars.Columns[6].Title.Caption := GetText(LOOKUP_SECTION, 'posset');
    SGFixStars.Columns[7].Title.Caption := GetText(LOOKUP_SECTION, 'posanticulm');

    SGMatches.Columns[0].Title.Caption := GetText(SECTION, 'colbasepos');
    SGMatches.Columns[1].Title.Caption := GetText(SECTION, 'colbasedatetime');
    SGMatches.Columns[2].Title.Caption := GetText(SECTION, 'colmatchbody');
    SGMatches.Columns[3].Title.Caption := GetText(SECTION, 'colmatchpos');
    SGMatches.Columns[4].Title.Caption := GetText(SECTION, 'colmatchdatetime');

  end;
end;

procedure TFormCentral.DefineButtons;
begin
  with Rosetta do begin
    BtnNew.Caption := GetText(SECTION, 'newcalc');
    BtnHelp.Caption := GetText(SHARED_SECTION, 'help');
    BtnExit.Caption := GetText(SHARED_SECTION, 'exit');
  end;
end;

procedure TFormCentral.HandleRequests;
var
  Orbis: double;
  FixStarSet: TEnumFixStarSets;
  SolSysPointSet: TEnumSolSysPointSets;
  HouseSystem: string;
  CalcApi: TAllParansApi;
  MatchesApi: TParanMatchesApi;
begin
  Orbis := CenCon.Settings.OrbSeconds / 86400;    // define orb as part of a full day
  ChartData := CenCon.ChartData;
  if (ChartData <> nil) then begin
    CalcApi := TAllParansApi.Create;
    MatchesApi := TParanMatchesApi.Create;
    FixStarSet := ptolemy;
    if CenCon.Settings.ModernPlanets then SolSysPointSet := modern
    else
      SolSysPointSet := classical;
    HouseSystem := 'W';
    ParansRequest := TParansPosRequest.Create(FixStarSet, SolSysPointSet, ChartData.JulianDay,
      ChartData.Location, HouseSystem, CenCon.Settings);
    ParansResponse := CalcApi.GetParans(ParansRequest);
    ParanMatchesRequest := TParansMatchesRequest.Create(ParansResponse.ParanCalculationResult.SolSysPoints,
      ParansResponse.ParanCalculationResult.FixStars, Orbis);
    ParanMatchesResponse := MatchesApi.FindMatches(ParanMatchesRequest);
  end;
end;

procedure TFormCentral.ShowData;
var
  SolSysPoints, FixStars: TFullParanPosArray;
  i, row: integer;
  Offset: double;
  Dst, GregCal: boolean;
  JdApi: TJulianDayApi;
  TextRow: array[0..8] of string;
begin
  JdApi := TJulianDayApi.Create;
  Offset := Cencon.InputValues.TimezoneOffset;
  Dst := CenCon.InputValues.Dst;
  GregCal := CenCon.InputValues.GregCal;
  SGCelPoints.Clean;
  row := 1;
  SolSysPoints := ParansResponse.ParanCalculationResult.SolSysPoints;
  for i := 0 to Length(SolSysPoints) - 1 do begin
    TextRow[0] := Rosetta.GetText(LOOKUP_SECTION, 'celpoint' + SolSysPoints[i].CelPointSpec.Name);
    TextRow[1] := SexagPos.FormatToText(SolSysPoints[i].FullEclPos[0], 3, dms);            // longitude
    TextRow[2] := SexagPos.FormatToText(SolSysPoints[i].EquatorialPos[1], 2, dms);         // declination
    TextRow[3] := SexagPos.FormatToText(SolSysPoints[i].HorizontalPos[1], 2, dms);         // altitude
    TextRow[4] := SexagPos.FormatToText(SolSysPoints[i].HorizontalPos[0], 3, dms);         // azimuth
    TextRow[5] := JdApi.FullDateTimeFromJd(SolSysPoints[i].ParanTimes[0], Offset, Dst, Gregcal).FormattedMDTime;
    TextRow[6] := JdApi.FullDateTimeFromJd(SolSysPoints[i].ParanTimes[1], Offset, Dst, Gregcal).FormattedMDTime;
    TextRow[7] := JdApi.FullDateTimeFromJd(SolSysPoints[i].ParanTimes[2], Offset, Dst, Gregcal).FormattedMDTime;
    TextRow[8] := JdApi.FullDateTimeFromJd(SolSysPoints[i].ParanTimes[3], Offset, Dst, Gregcal).FormattedMDTime;
    SGCelPoints.InsertRowWithValues(row, TextRow);
    Inc(row);
  end;
  SGFixStars.Clean;
  row := 1;
  FixStars := ParansResponse.ParanCalculationResult.FixStars;
  for i := 0 to Length(FixStars) - 1 do begin
    TextRow[0] := FixStars[i].CelPointSpec.Name;
    TextRow[1] := SexagPos.FormatToText(FixStars[i].FullEclPos[0], 3, dms);            // longitude
    TextRow[2] := SexagPos.FormatToText(FixStars[i].EquatorialPos[1], 2, dms);         // declination
    TextRow[3] := SexagPos.FormatToText(FixStars[i].HorizontalPos[1], 2, dms);         // altitude
    TextRow[4] := SexagPos.FormatToText(FixStars[i].HorizontalPos[0], 3, dms);         // azimuth
    TextRow[5] := JdApi.FullDateTimeFromJd(FixStars[i].ParanTimes[0], Offset, Dst, Gregcal).FormattedMDTime;
    TextRow[6] := JdApi.FullDateTimeFromJd(FixStars[i].ParanTimes[1], Offset, Dst, Gregcal).FormattedMDTime;
    TextRow[7] := JdApi.FullDateTimeFromJd(FixStars[i].ParanTimes[2], Offset, Dst, Gregcal).FormattedMDTime;
    TextRow[8] := JdApi.FullDateTimeFromJd(FixStars[i].ParanTimes[3], Offset, Dst, Gregcal).FormattedMDTime;
    SGFixStars.InsertRowWithValues(row, TextRow);
    Inc(row);
  end;

end;

procedure TFormCentral.ShowMatches;
var
  i, j, row: integer;
  Offset: double;
  Dst, GregCal: boolean;
  JdApi: TJulianDayApi;
  MatchesArray: TParanMatchesArray;
  CandidateMatches: TParanDataArray;
  TransLationHelper: TTranslationHelper;
  TextRow: array[0..5] of string;
begin
  TransLationHelper := TTranslationHelper.Create;
  JdApi := TJulianDayApi.Create;
  Offset := Cencon.InputValues.TimezoneOffset;
  Dst := CenCon.InputValues.Dst;
  GregCal := CenCon.InputValues.GregCal;

  SgMatches.Clean;
  row := 1;
  MatchesArray := ParanMatchesResponse.ParanMatches;
  for i := 0 to Length(MatchesArray) - 1 do begin
    TextRow[0] := Rosetta.GetText(LOOKUP_SECTION, 'celpoint' + MatchesArray[i].Base.CelPointSpec.Name);
    TextRow[1] := TransLationHelper.ParanPos2TranslatedText(MatchesArray[i].Base.ParanPos);
    TextRow[2] := JdApi.FullDateTimeFromJd(MatchesArray[i].Base.JulianDay, Offset, Dst, Gregcal).FormattedMDTime;
    for j := 0 to Length(MatchesArray[i].Matches) - 1 do begin
      CandidateMatches := MatchesArray[i].Matches;
      TextRow[3] := CandiDateMatches[j].CelPointSpec.Name;
      TextRow[4] := TransLationHelper.ParanPos2TranslatedText(CandidateMatches[j].ParanPos);
      TextRow[5] := JdApi.FullDateTimeFromJd(CandidateMatches[j].JulianDay, Offset, Dst, Gregcal).FormattedMDTime;
      SgMatches.InsertRowWithValues(row, TextRow);
    end;
    Inc(row);
  end;

end;


end.
