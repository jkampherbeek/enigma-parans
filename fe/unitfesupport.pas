{ Jan Kampherbeek, (c)  2021.
  Enigma Parans is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit unitfesupport;

{< Support classes for the frontend. }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, UnitApi, UnitDomainXchg, UnitTranslation;

type

  TConvertedInts = record
    ThreeInts: array of integer;
    Valid: boolean;
  end;

  { TTranslationHelper }

  TTranslationHelper = class
  strict private
    Rosetta: TRosetta;
  public
    constructor Create;
    destructor Destroy; override;
    function ParanPos2TranslatedText(PParanPos: TEnumParanPositions): string;
  end;


  { TTextConverter }

  TTextConverter = class
  strict private
    ErrorCode: integer;
  public
    { Converts an array with three texts to an array with three integers.
      If the arraylength <> 3 or if the texts contain non-numeric values, the resulting TConvertedInts.Valid is false. }
    function ThreeTexts2ThreeInts(PTexts: array of string): TConvertedInts;
  end;

  { TSexagesimalConverter }

  TSexagesimalConverter = class
  public
    function ThreeInts2Decimal(PDegreesHours, PMinutes, PSeconds, PMinLimitDH, PMaxLimitDH: integer): TValidatedDouble;
  end;

  { TChartDataValidator }

  TChartDataValidator = class
  strict private
    TextConverter: TTextConverter;
    SexagesimalConverter: TSexagesimalConverter;
    JulianDayApi: TJulianDayApi;
    ThreeTexts: array of string;
    ConvertedInts: TConvertedInts;
    ValidatedDouble: TValidatedDouble;
    function HandleGeoCoordinate(PDegrees, PMinutes, PSeconds: string; PDirection: TGeoDirSpec;
      MinDegree, MaxDegree: integer): TValidatedDouble;
  public
    constructor Create;
    destructor Destroy; override;
    function HandleLongitude(PDegrees, PMinutes, PSeconds: string; PDirection: TGeoDirSpec): TValidatedDouble;
    function HandleLatitude(PDegrees, PMinutes, PSeconds: string; PDirection: TGeoDirSpec): TValidatedDouble;
    function HandleAltitude(PMeters: string): TValidatedInteger;
    function HandleDate(PYear, PMonth, PDay: string; PCalendar: TCalendarSpec;
      PYearCount: TYearCountSpec): TValidatedDouble;
    function HandleTime(PHour, PMinute, PSecond: string): TValidatedDouble;
  end;

  { TSettingsValidator }

  TSettingsValidator = class
  strict private
      errorCode: integer;
  valInt: TValidatedInteger;
  public
    function HandleMinutes(PMinutes: string): TValidatedInteger;
    function HandleSeconds(PSeconds: string): TValidatedInteger;
  end;

implementation

uses
  unitconst;

{ TSettingsValidator }


function TSettingsValidator.HandleMinutes(PMinutes: string): TValidatedInteger;
begin
  valInt.valid:= true;
  Val(PMinutes, valInt.Value, errorCode);
  if (ErrorCode <> 0) then valInt.Valid := False else
  if ((valInt.Value > MAX_MINUTE_ORB_IN_TIME) or (valInt.Value < MIN_MINUTE_ORB_IN_TIME)) then valint.Valid:= false;
  Result:= valInt;
end;

function TSettingsValidator.HandleSeconds(PSeconds: string): TValidatedInteger;
begin
  valInt.valid:= true;
  Val(PSeconds, valInt.Value, errorCode);
  if (ErrorCode <> 0) then valInt.Valid := False else
  if ((valInt.Value > MAX_SECOND) or (valInt.Value < MIN_SECOND)) then valint.Valid:= false;
  Result:= valInt;
end;

{ TTranslationHelper }

constructor TTranslationHelper.Create;
begin
  Rosetta:= TRosetta.Create;
end;

destructor TTranslationHelper.Destroy;
begin
  FreeAndNil(Rosetta);
  inherited Destroy;
end;

function TTranslationHelper.ParanPos2TranslatedText(PParanPos: TEnumParanPositions): string;
var
  RbName: string;
begin
  case PParanPos of
    rising: RbName:= 'paranpos.rise';
    setting: RbName:= 'paranpos.set';
    culminating: RbName:= 'paranpos.culm';
    anticulminating: RbName:= 'paranpos.anticulm';
    otherwise RbName:= '';
  end;
  Result:= Rosetta.GetText('shared', RbName);
end;


{ TSexagesimalConverter }

function TSexagesimalConverter.ThreeInts2Decimal(PDegreesHours, PMinutes, PSeconds, PMinLimitDH,
  PMaxLimitDH: integer): TValidatedDouble;
var
  ValDouble: TValidatedDouble;
  PosNegSign: integer;
  DegreesHours: double;
begin
  DegreesHours := Abs(PDegreesHours);
  if (PDegreesHours < 0) then PosNegSign := -1
  else
    PosNegSign := 1;
  ValDouble.Valid := True;
  if (PMinLimitDH > PDegreesHours) or (PMaxLimitDH < PDegreesHours) or (MIN_MINUTE > PMinutes) or
    (MAX_MINUTE < PMinutes) or (MIN_SECOND > PSeconds) or (MAX_SECOND < PSeconds) then ValDouble.Valid := False;
  if (ValDouble.Valid) then ValDouble.Value :=
      (DegreesHours + (PMinutes / MINUTES_IN_HOUR) + (PSeconds / SECONDS_IN_HOUR)) * PosNegSign;
  Result := ValDouble;
end;

{ TTextConverter }

function TTextConverter.ThreeTexts2ThreeInts(PTexts: array of string): TConvertedInts;
var
  ConvertedInts: TConvertedInts;
  i: integer;
begin
  ConvertedInts.ThreeInts := [0, 0, 0];
  ConvertedInts.Valid := Length(PTexts) = 3;
  if (ConvertedInts.Valid) then for i := 0 to 2 do begin
      Val(PTexts[i], ConvertedInts.ThreeInts[i], ErrorCode);
      if (ErrorCode <> 0) then ConvertedInts.Valid := False;
    end;
  Result := ConvertedInts;
end;


{ TChartDataValidator }

constructor TChartDataValidator.Create;
begin
  TextConverter := TTextConverter.Create;
  SexagesimalConverter := TSexagesimalConverter.Create;
  JulianDayApi := TJulianDayApi.Create;
  SetLength(ThreeTexts, 3);
end;

destructor TChartDataValidator.Destroy;
begin
  FreeAndNil(TextConverter);
  FreeAndNil(SexagesimalConverter);
  FreeAndNil(JulianDayApi);
  inherited Destroy;
end;

function TChartDataValidator.HandleGeoCoordinate(PDegrees, PMinutes, PSeconds: string;
  PDirection: TGeoDirSpec; MinDegree, MaxDegree: integer): TValidatedDouble;
begin
  ThreeTexts[0] := PDegrees;
  ThreeTexts[1] := PMinutes;
  ThreeTexts[2] := PSeconds;
  ConvertedInts := TextConverter.ThreeTexts2ThreeInts(ThreeTexts);
  if (ConvertedInts.Valid) then begin
    ValidatedDouble := SexagesimalConverter.ThreeInts2Decimal(ConvertedInts.ThreeInts[0],
      ConvertedInts.ThreeInts[1], ConvertedInts.ThreeInts[2], MinDegree, MaxDegree);
    ValidatedDouble.Value := ValidatedDouble.Value * PDirection.PlusMinus;
  end else
    ValidatedDouble.Valid := False;
  Result := ValidatedDouble;
end;

function TChartDataValidator.HandleLongitude(PDegrees, PMinutes, PSeconds: string;
  PDirection: TGeoDirSpec): TValidatedDouble;
var
  ValLongitude: TValidatedDouble;
begin
  ValLongitude := HandleGeoCoordinate(PDegrees, PMinutes, PSeconds, PDirection, MIN_LONGITUDE_DEGREES,
    MAX_LONGITUDE_DEGREES);
  if (ValLongitude.Value > 180.0) then ValLongitude.Valid:= false;
  Result:= ValLongitude;
end;

function TChartDataValidator.HandleLatitude(PDegrees, PMinutes, PSeconds: string;
  PDirection: TGeoDirSpec): TValidatedDouble;
begin
  Result := HandleGeoCoordinate(PDegrees, PMinutes, PSeconds, PDirection, MIN_LATITUDE_DEGREES, MAX_LATITUDE_DEGREES);
end;

function TChartDataValidator.HandleAltitude(PMeters: string): TValidatedInteger;
var
  ValResult: TValidatedInteger;
  ErrorCode: integer;
begin
  if (Length(PMeters) = 0) then begin
    ValResult.Value:= 0;
    ValResult.Valid:= true;
  end else
  begin
    Val(PMeters, ValResult.Value, ErrorCode);
    ValResult.Valid := (ErrorCode = 0);
  end;
  Result:= ValResult;
end;

function TChartDataValidator.HandleDate(PYear, PMonth, PDay: string; PCalendar: TCalendarSpec;
  PYearCount: TYearCountSpec): TValidatedDouble;
begin
  ThreeTexts[0] := PYear;
  ThreeTexts[1] := PMonth;
  ThreeTexts[2] := PDay;
  ConvertedInts := TextConverter.ThreeTexts2ThreeInts(ThreeTexts);
  if (ConvertedInts.Valid) then begin
    if (PYearCount.Index = 1) then ConvertedInts.ThreeInts[0] := (ConvertedInts.ThreeInts[0] - 1) * -1;    // Handle BCE
    ValidatedDouble :=
      JulianDayApi.CheckAndHandleDate(ConvertedInts.ThreeInts[0], ConvertedInts.ThreeInts[1],
      ConvertedInts.ThreeInts[2], PCalendar.Index = 0);   // Greg = 0
  end else
    ValidatedDouble.Valid := False;
  Result := ValidatedDouble;
end;

function TChartDataValidator.HandleTime(PHour, PMinute, PSecond: string): TValidatedDouble;
begin
  ThreeTexts[0] := PHour;
  ThreeTexts[1] := PMinute;
  ThreeTexts[2] := PSecond;
  ConvertedInts := TextConverter.ThreeTexts2ThreeInts(ThreeTexts);
  if (ConvertedInts.Valid) then ValidatedDouble :=
      SexagesimalConverter.ThreeInts2Decimal(ConvertedInts.ThreeInts[0], ConvertedInts.ThreeInts[1],
      ConvertedInts.ThreeInts[2], MIN_HOUR, MAX_HOUR)
  else
    ValidatedDouble.Valid := False;
  Result := ValidatedDouble;
end;


end.
