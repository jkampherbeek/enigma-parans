{ Jan Kampherbeek, (c)  2021.
  Enigma Parans is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit UnitDataInput;

{< Form for data input.}

{$mode objfpc}{$H+}

interface

uses
  BCListBox, Classes, Controls, Dialogs, ExtCtrls,
  Forms, Graphics, Ipfilebroker, IpHtml, LazHelpHTML, Menus, StdCtrls, SysUtils, HelpIntfs,
  unitcentralcontroller, UnitCentralForm, UnitAbout, UnitDomainXchg, unitfesupport, UnitLog, unitreferences,
  unitsettings, unittranslation;

type

  { TFormDataInput }

  TFormDataInput = class(TForm)
    BtnCalc: TButton;
    BtnExit: TButton;
    BtnHelp: TButton;
    CBDST: TCheckBox;
    CMBCalendar: TComboBox;
    CMBLatDir: TComboBox;
    CMBLongLmtDir: TComboBox;
    CMBLonDir: TComboBox;
    CMBTimeZones: TComboBox;
    CMBYearCount: TComboBox;
    EditAltitude: TEdit;
    EditLatD: TEdit;
    EditLongLmtD: TEdit;
    EditLongLmtM: TEdit;
    EditLongLmtS: TEdit;
    EditTimeH: TEdit;
    EditLatM: TEdit;
    EditTimeM: TEdit;
    EditLatS: TEdit;
    EditDateDay: TEdit;
    EditDateMonth: TEdit;
    EditTimeS: TEdit;
    EditLonD: TEdit;
    EditLonM: TEdit;
    EditLonS: TEdit;
    EditNameDescr: TEdit;
    EditDateYear: TEdit;
    HTMLBrowserHelpViewer1: THTMLBrowserHelpViewer;
    HTMLHelpDatabase1: THTMLHelpDatabase;
    LblAltitude: TLabel;
    LblDateCalendar: TLabel;
    LblDateDay: TLabel;
    LblDateMonth: TLabel;
    LblDateYear: TLabel;
    LblDateYearCount: TLabel;
    LblLmtDegree: TLabel;
    LblLmtMinute: TLabel;
    LblLmtSecond: TLabel;
    LblTimeHour: TLabel;
    LblTimeMinute: TLabel;
    LblTimeSecond: TLabel;
    LblDegLat: TLabel;
    LblDegLon: TLabel;
    LblDate: TLabel;
    LblGeoLat: TLabel;
    LblGeoLon: TLabel;
    LblLongLmt: TLabel;
    LblMeters: TLabel;
    LblMinLat: TLabel;
    LblMinLon: TLabel;
    LblNameDescr: TLabel;
    LblSecLat: TLabel;
    LblSecLon: TLabel;
    LblTime: TLabel;
    LblTimezone: TLabel;
    LblTitle: TLabel;
    MenuDataInput: TMainMenu;
    MISettings: TMenuItem;
    MIDutch: TMenuItem;
    MICurrentHelp: TMenuItem;
    MIAbout: TMenuItem;
    MIEnglish: TMenuItem;
    MIHelp: TMenuItem;
    MICommon: TMenuItem;
    MIExit: TMenuItem;
    MILanguage: TMenuItem;
    procedure BtnCalcClick(Sender: TObject);
    procedure BtnExitClick(Sender: TObject);
    procedure BtnHelpClick(Sender: TObject);
    procedure CMBTimeZonesChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MIAboutClick(Sender: TObject);
    procedure MIDutchClick(Sender: TObject);
    procedure MIEnglishClick(Sender: TObject);
    procedure MISettingsClick(Sender: TObject);
  private
    Rosetta: TRosetta;
    References: TParanRefs;
    NameDescr: string;
    GeoLat, GeoLong, Altitude, TimezoneOffset, JD4Day, JD4Time, JulianDay: double;
    ChartDataValidator: TChartDataValidator;
    CenCon: TCenCon;
    Logger: TLogger;
    InputValid: boolean;
    procedure DefineLongLmt(PEnabled: boolean);
    function DefinePresInputData: TPresentableInputData;
    procedure Populate;
    procedure PopulateGeneral;
    procedure PopulateLocation;
    procedure PopulateDate;
    procedure PopulateTime;
    procedure PopulateButtons;
    procedure PopulateMenu;
    procedure ProcessNameDescription;
    procedure ProcessLongitude;
    procedure ProcessLatitude;
    procedure ProcessAltitude;
    procedure ProcessDate;
    procedure ProcessTime;
    procedure ProcessTimeZone;
  public

  end;

var
  FormDataInput: TFormDataInput;

implementation

uses
  unitconst;

const
  SECTION = 'formdatainput';


{$R *.lfm}

{ TFormDataInput }


procedure TFormDataInput.FormCreate(Sender: TObject);
begin
  CenCon := TCenCon.Create;
  Rosetta := TRosetta.Create;
  References := TParanRefs.Create;
  ChartDataValidator := TChartDataValidator.Create;
  Logger := TLogger.Create;
  Logger.log('Enigma Parans startup.');
end;

procedure TFormDataInput.CMBTimeZonesChange(Sender: TObject);
begin
  if (CMBTimeZones.ItemIndex = 32) then DefineLongLmt(True)
  else
    DefineLongLmt(False);
end;

procedure TFormDataInput.BtnCalcClick(Sender: TObject);
var
  ChartData: TChartData;
  InputValues: TInputValues;
begin
  InputValid := True;
  ProcessNameDescription;
  ProcessLatitude;
  ProcessLongitude;
  ProcessAltitude;
  ProcessDate;
  ProcessTime;
  JulianDay := JD4Day + JD4Time;
  if (InputValid) then begin
    ChartData := TChartData.Create(NameDescr, GeoLong, GeoLat, Altitude, JulianDay);
    CenCon.SetChartData(ChartData);
    CenCon.SetPresInputData(DefinePresInputData);
    InputValues := TInputValues.Create(TimezoneOffset, CBDST.Checked, CMBCalendar.ItemIndex = 0);
    Cencon.SetInputValues(InputValues);
    FormCentral.Show;
  end;
end;

procedure TFormDataInput.BtnExitClick(Sender: TObject);
begin
  Halt;
end;

procedure TFormDataInput.BtnHelpClick(Sender: TObject);
var
  HelpText: string;
begin
  if Rosetta.CurrentLanguage = 'en' then HelpText := 'html/en_datainputform.html'
  else
    HelpText := 'html/nl_datainputform.html';
  ShowHelpOrErrorForKeyword(EmptyStr, HelpText);

end;

procedure TFormDataInput.FormShow(Sender: TObject);
begin
  Populate;
end;

procedure TFormDataInput.MIAboutClick(Sender: TObject);
begin
  FormAbout.ShowModal;
end;

procedure TFormDataInput.MIDutchClick(Sender: TObject);
begin
  Rosetta.ChangeLanguage('nl');
  FormShow(Sender);
end;

procedure TFormDataInput.MIEnglishClick(Sender: TObject);
begin
  Rosetta.ChangeLanguage('en');
  FormShow(Sender);
end;

procedure TFormDataInput.MISettingsClick(Sender: TObject);
begin
  FormSettings.ShowModal;
end;


procedure TFormDataInput.DefineLongLmt(PEnabled: boolean);
begin
  LblLongLmt.Enabled := PEnabled;
  EditLongLmtD.Enabled := PEnabled;
  EditLongLmtM.Enabled := PEnabled;
  EditLongLmtS.Enabled := PEnabled;
  CMBLongLmtDir.Enabled := PEnabled;
  EditLongLmtD.ShowHint := PEnabled;
  EditLongLmtM.ShowHint := PEnabled;
  EditLongLmtS.ShowHint := PEnabled;
  CMBLongLmtDir.ShowHint := PEnabled;
end;

function TFormDataInput.DefinePresInputData: TPresentableInputData;
var
  PresInputData: TPresentableInputData;
  Cal, YearCount, Zone, OffsetTxt, DstText, HeightText: string;
begin
  PresInputData.NameDescr := EditNameDescr.Caption;
  if (CMBCalendar.ItemIndex = 0) then Cal := Rosetta.GetText(SHARED_SECTION, 'gregorian')
  else
    Cal := Rosetta.GetText(SHARED_SECTION, 'julian');
  if (CMBYearCount.ItemIndex = 0) then YearCount := Rosetta.GetText(SHARED_SECTION, 'yearcountce')
  else if (CMBYearCount.ItemIndex = 1) then YearCount := Rosetta.GetText(SHARED_SECTION, 'yearcountbce')
  else
    YearCount := Rosetta.GetText(SHARED_SECTION, 'yearcountastron');
  if (CMBTimeZones.ItemIndex = 32) then OffSetTxt :=
      ' (' + EditLongLmtD.Caption + DEGREE_SIGN + EditLongLmtM.Caption + MINUTE_SIGN +
      EditLongLmtS.Caption + SECOND_SIGN + CMBLongLmtDir.Caption + ') '
  else
    OffsetTxt := '';
  Zone := CMBTimeZones.Caption + OffsetTxt;
  if (CBDST.Checked) then DstText := Rosetta.GetText(SECTION, 'dst')
  else
    DstText := '';
  PresInputData.DateTime := EditDateYear.Caption + '/' + EditDateMonth.Caption + '/' +
    EditDateDay.Caption + ' ' + Cal + ' ' + YearCount + ' ' + EditTimeH.Caption + ':' +
    EditTimeM.Caption + ':' + EditTimeS.Caption + ' ' + Zone + ' ' + DstText;
  if (EditAltitude.Caption <> '') then HeightText :=
      ' / ' + Rosetta.GetText(SECTION, 'altitude') + ': ' + EditAltitude.Caption
  else
    HeightText := '';
  PresInputData.Location := EditLatD.Caption + DEGREE_SIGN + EditLatM.Caption + MINUTE_SIGN +
    EditLatS.Caption + SECOND_SIGN + ' ' + CmbLatDir.Caption + ' / ' + EditLonD.Caption +
    DEGREE_SIGN + EditLonM.Caption + MINUTE_SIGN + EditLonS.Caption + SECOND_SIGN + ' ' +
    CMBLonDir.Caption + HeightText;
  Result := PresInputData;
end;

procedure TFormDataInput.Populate;
begin
  PopulateGeneral;
  PopulateLocation;
  PopulateDate;
  PopulateTime;
  PopulateButtons;
  PopulateMenu;
end;

procedure TFormDataInput.PopulateGeneral;
begin
  with Rosetta do begin
    Caption:= GetText(SECTION, 'formtitle');
    LblTitle.Caption := GetText(SECTION, 'formtitle');
    LblNameDescr.Caption := GetText(SECTION, 'namedescription');
    EditNameDescr.Hint := GetText(SECTION, 'hintnamedescr');
  end;
end;

procedure TFormDataInput.PopulateLocation;
var
  i: integer;
begin
  with Rosetta do begin
    { Latitude }
    LblDegLat.Caption := GetText(SHARED_SECTION, 'inputdegree');
    LblMinLat.Caption := GetText(SHARED_SECTION, 'inputminute');
    LblSecLat.Caption := GetText(SHARED_SECTION, 'inputsecond');
    LblGeoLat.Caption := GetText(SECTION, 'geolat');
    EditLatD.Hint := GetText(SHARED_SECTION, 'hintdegree2pos');
    EditLatM.Hint := GetText(SHARED_SECTION, 'hintminute');
    EditLatS.Hint := GetText(SHARED_SECTION, 'hintsecond');
    CMBLatDir.Items.Clear;
    for i := 0 to Length(References.AllLatitudeDirections) - 1 do CMBLatDir.Items[i] :=
        GetText(LOOKUP_SECTION, References.AllLatitudeDirections[i].Name);
    CMBLatDir.ItemIndex := 0;
    CMBLatDir.Hint := GetText(SHARED_SECTION, 'hintlatdirection');
    { Longitude }
    LblDegLon.Caption := GetText(SHARED_SECTION, 'inputdegree');
    LblMinLon.Caption := GetText(SHARED_SECTION, 'inputminute');
    LblSecLon.Caption := GetText(SHARED_SECTION, 'inputsecond');
    LblGeoLon.Caption := GetText(SECTION, 'geolong');
    EditLonD.Hint := GetText(SHARED_SECTION, 'hintdegree3pos');
    EditLonM.Hint := GetText(SHARED_SECTION, 'hintminute');
    EditLonS.Hint := GetText(SHARED_SECTION, 'hintsecond');
    CMBLonDir.Items.Clear;
    for i := 0 to Length(References.AllLongitudeDirections) - 1 do CMBLonDir.Items[i] :=
        GetText(LOOKUP_SECTION, References.AllLongitudeDirections[i].Name);
    CMBLonDir.ItemIndex := 0;
    CMBLonDir.Hint := GetText(SHARED_SECTION, 'hintlondirection');
    { Altitude }
    LblAltitude.Caption := GetText(SECTION, 'altitude');
    LblMeters.Caption := GetText(SECTION, 'meters');
    EditAltitude.Hint := GetText(SECTION, 'hintaltitude');
  end;
end;

procedure TFormDataInput.PopulateDate;
var
  i: integer;
begin
  with Rosetta do begin
    { Date }
    LblDateYear.Caption := GetText(SHARED_SECTION, 'inputyear');
    LblDateMonth.Caption := GetText(SHARED_SECTION, 'inputmonth');
    LblDateDay.Caption := GetText(SHARED_SECTION, 'inputday');
    LblDateCalendar.Caption := GetText(SHARED_SECTION, 'inputcalendar');
    LblDateYearCount.Caption := GetText(SHARED_SECTION, 'inputyearcount');
    LblDate.Caption := GetText(SECTION, 'date');
    EditDateYear.Hint := GetText(SHARED_SECTION, 'hintyear');
    EditDateMonth.Hint := GetText(SHARED_SECTION, 'hintmonth');
    EditDateDay.Hint := GetText(SHARED_SECTION, 'hintday');
    CMBCalendar.Hint := GetText(SHARED_SECTION, 'hintcalendar');
    CMBCalendar.Items.Clear;
    for i := 0 to Length(References.AllCalendars) - 1 do CMBCalendar.Items[i] :=
        GetText(SHARED_SECTION, References.AllCalendars[i].Name);
    CMBCalendar.ItemIndex := 0;
    CMBYearCount.Hint := GetText(SHARED_SECTION, 'hintyearcount');
    CMBYearCount.Items.Clear;
    for i := 0 to Length(References.AllYearCounts) - 1 do CMBYearCount.Items[i] :=
        GetText(SHARED_SECTION, References.AllYearCounts[i].Name);
    CMBYearCount.ItemIndex := 0;
  end;
end;

procedure TFormDataInput.PopulateTime;
var
  i: integer;
begin
  with Rosetta do begin
    { Time }
    LblTimeHour.Caption := GetText(SHARED_SECTION, 'inputhour');
    LblTimeMinute.Caption := GetText(SHARED_SECTION, 'inputminute');
    LblTimeSecond.Caption := GetText(SHARED_SECTION, 'inputsecond');
    LblTime.Caption := GetText(SECTION, 'time');
    EditTimeH.Hint := GetText(SHARED_SECTION, 'hinthour');
    EditTimeM.Hint := GetText(SHARED_SECTION, 'hintminute');
    EditTimeS.Hint := GetText(SHARED_SECTION, 'hintsecond');
    LblTimezone.Caption := GetText(SECTION, 'timezone');
    CBDST.Caption := GetText(SECTION, 'dst');
    CBDST.Hint := GetText(SHARED_SECTION, 'hintdst');
    { TimeZones }
    CMBTimeZones.Items.Clear;
    for i := 0 to Length(References.AllTimeZones) - 1 do CMBTimeZones.Items[i] :=
        GetText(LOOKUP_SECTION, References.AllTimeZones[i].Name);
    CMBTimeZones.ItemIndex := 0;
    CMBTimeZones.Hint := GetText(SHARED_SECTION, 'hinttimezone');
    LblLongLMT.Caption := GetText(SECTION, 'longlmt');
    LblLmtDegree.Caption := GetText(SHARED_SECTION, 'inputdegree');
    LblLmtMinute.Caption := GetText(SHARED_SECTION, 'inputminute');
    LblLmtSecond.Caption := GetText(SHARED_SECTION, 'inputsecond');
    CMBLongLmtDir.Items.Clear;
    for i := 0 to Length(References.AllLongitudeDirections) - 1 do CMBLongLmtDir.Items[i] :=
        GetText(LOOKUP_SECTION, References.AllLongitudeDirections[i].Name);
    CMBLongLmtDir.ItemIndex := 0;
    EditLongLmtD.Hint := GetText(SHARED_SECTION, 'hintdegree3pos');
    EditLongLmtM.Hint := GetText(SHARED_SECTION, 'hintminute');
    EditLongLmtS.Hint := GetText(SHARED_SECTION, 'hintsecond');
    CMBLongLmtDir.Hint := GetText(SHARED_SECTION, 'hintlondirection');
    CBDST.Checked := False;
    DefineLongLmt(False);
  end;
end;

procedure TFormDataInput.PopulateButtons;
begin
  with Rosetta do begin
    { Buttons }
    BtnCalc.Caption := GetText(SHARED_SECTION, 'calculate');
    BtnHelp.Caption := GetText(SHARED_SECTION, 'help');
    BtnExit.Caption := GetText(SHARED_SECTION, 'exit');
  end;
end;

procedure TFormDataInput.PopulateMenu;
begin
  with Rosetta do begin
    { Menu }
    MICommon.Caption := GetText(MENU_SECTION, 'micommon');
    MISettings.Caption := GetText(MENU_SECTION, 'micommon.settings');
    MIExit.Caption := GetText(MENU_SECTION, 'micommon.exit');
    MILanguage.Caption := GetText(MENU_SECTION, 'milanguage');
    MIEnglish.Caption := GetText(MENU_SECTION, 'milanguage.english');
    MIDutch.Caption := GetText(MENU_SECTION, 'milanguage.dutch');
    MIHelp.Caption := GetText(MENU_SECTION, 'mihelp');
    MICurrentHelp.Caption := GetText(MENU_SECTION, 'mihelp.currenthelp');
    MIAbout.Caption := GetText(MENU_SECTION, 'mihelp.about');
  end;
end;

procedure TFormDataInput.ProcessNameDescription;
begin
  NameDescr := Trim(EditNameDescr.Text);    // empty string is covered in FormMain
end;

procedure TFormDataInput.ProcessLongitude;
var
  ValidatedDouble: TValidatedDouble;
  Index: integer;
  GeoDirSpec: TGeoDirSpec;
begin
  EditLonD.Color := clDefault;
  EditLonM.Color := clDefault;
  EditLonS.Color := clDefault;
  if (Length(Trim(EditLonS.Text)) = 0) then EditLonS.Text := '0';
  Index := CMBLonDir.ItemIndex;
  GeoDirSpec := References.AllLatitudeDirections[Index];
  ValidatedDouble := ChartDataValidator.HandleLongitude(EditLonD.Text, EditLonM.Text, EditLonS.Text, GeoDirSpec);
  if (ValidatedDouble.Valid) then GeoLong := ValidatedDouble.Value
  else begin
    InputValid := False;
    EditLonD.Color := clYellow;
    EditLonM.Color := clYellow;
    EditLonS.Color := clYellow;
    Logger.log('Input error for longitude in TFormDataInput, values DMS: ' + EditLonD.Text +
      ', ' + EditLonM.Text + ', ' + EditLonS.Text);
  end;
end;


procedure TFormDataInput.ProcessLatitude;
var
  ValidatedDouble: TValidatedDouble;
  Index: integer;
  GeoDirSpec: TGeoDirSpec;
begin
  EditLatD.Color := clDefault;
  EditLatM.Color := clDefault;
  EditLatS.Color := clDefault;
  if (Length(Trim(EditLatS.Text)) = 0) then EditLatS.Text := '0';
  Index := CMBLatDir.ItemIndex;
  GeoDirSpec := References.AllLongitudeDirections[Index];
  ValidatedDouble := ChartDataValidator.HandleLatitude(EditLatD.Text, EditLatM.Text, EditLatS.Text, GeoDirSpec);
  if (ValidatedDouble.Valid) then GeoLat := ValidatedDouble.Value
  else begin
    InputValid := False;
    EditLatD.Color := clYellow;
    EditLatM.Color := clYellow;
    EditLatS.Color := clYellow;
    Logger.log('Input error for latitude in TFormDataInput, values DMS: ' + EditLatD.Text +
      ', ' + EditLatM.Text + ', ' + EditLatS.Text);
  end;
end;

procedure TFormDataInput.ProcessAltitude;
var
  ValidatedInteger: TValidatedInteger;
begin
  EditAltitude.Color := clDefault;
  ValidatedInteger := ChartDataValidator.HandleAltitude(EditAltitude.Text);
  if (ValidatedInteger.Valid) then Altitude := ValidatedInteger.Value
  else begin
    InputValid := False;
    EditAltitude.Color := clYellow;
    Logger.log('Input error for altitude in TFormDataInput, value: ' + EditAltitude.Text);
  end;
end;


procedure TFormDataInput.ProcessDate;
var
  ValidatedDouble: TValidatedDouble;
  Index: integer;
  Calendar: TCalendarSpec;
  YearCount: TYearCountSpec;
begin
  EditDateDay.Color := clDefault;
  EditDateMonth.Color := clDefault;
  EditDateYear.Color := clDefault;
  Index := CMBCalendar.ItemIndex;
  Calendar := References.AllCalendars[Index];
  Index := CMBYearCount.ItemIndex;
  YearCount := References.AllYearCounts[Index];
  ValidatedDouble := ChartDataValidator.HandleDate(EditDateYear.Text, EditDateMonth.Text,
    EditDateDay.Text, Calendar, Yearcount);
  if (ValidatedDouble.Valid) then JD4Day := ValidatedDouble.Value
  else begin
    InputValid := False;
    EditDateDay.Color := clYellow;
    EditDateMonth.Color := clYellow;
    EditDateYear.Color := clYellow;
    Logger.log('Input error for date in TFormDataInput, values YMD,cal,yearcount: ' +
      EditDateDay.Text + ', ' + EditDateMonth.Text + ', ' + EditDateYear.Text + ', ' +
      Calendar.Name + ', ' + YearCount.Name);
  end;
end;

procedure TFormDataInput.ProcessTimeZone;
var
  ValidatedDouble: TValidatedDouble;
  Index: integer;
  TimeZoneSpec: TTimeZoneSpec;
  GeoDirSpec: TGeoDirSpec;
begin
  EditLongLmtD.Color := clDefault;
  EditLongLmtM.Color := clDefault;
  EditLongLmtS.Color := clDefault;
  Index := CMBTimeZones.ItemIndex;
  TimezoneSpec := References.AllTimeZones[Index];
  if (TimeZoneSpec.Name = 'tzlmt') then begin
    if (Length(Trim(EditLongLmtS.Text)) = 0) then EditLongLmtS.Text := '0';
    Index := CMBLongLmtDir.ItemIndex;
    GeoDirSpec := References.AllLongitudeDirections[Index];
    ValidatedDouble := ChartDataValidator.HandleLongitude(EditLongLmtD.Text, EditLongLmtM.Text,
      EditLongLmtS.Text, GeoDirSpec);
    if (ValidatedDouble.Valid) then TimezoneOffset := ValidatedDouble.Value / 15.0
    else begin
      InputValid := False;
      EditLongLmtD.Color := clYellow;
      EditLongLmtM.Color := clYellow;
      EditLongLmtS.Color := clYellow;
      Logger.log('Input error for longitude for LMT in TFormDataInput, values DMS: ' +
        EditLongLmtD.Text + ', ' + EditLongLmtM.Text + ', ' + EditLongLmtS.Text);
    end;
  end else
    TimezoneOffset := TimeZoneSpec.Offset;
end;

procedure TFormDataInput.ProcessTime;
var
  ValidatedDouble: TValidatedDouble;
  EnteredTime: double;
begin
  ProcessTimeZone;
  EditTimeH.Color := clDefault;
  EditTimeM.Color := clDefault;
  EditTimeS.Color := clDefault;
  if (Length(Trim(EditTimeS.Text)) = 0) then EditTimeS.Text := '0';
  ValidatedDouble := ChartDataValidator.HandleTime(EditTimeH.Text, EditTimeM.Text, EditTimeS.Text);
  if (ValidatedDouble.Valid) then begin
    EnteredTime := ValidatedDouble.Value;
    if (CBDST.Checked) then EnteredTime := EnteredTime - 1.0;
    EnteredTime := EnteredTime - TimezoneOffset;
    JD4Time := EnteredTime / 24.0;
  end else begin
    InputValid := False;
    EditTimeH.Color := clYellow;
    EditTimeM.Color := clYellow;
    EditTimeS.Color := clYellow;
    Logger.log('Input error for time in TFormDataInput, values HMS: ' + EditTimeH.Text + ', ' +
      EditTimeM.Text + ', ' + EditTimeS.Text);
  end;

end;

end.
