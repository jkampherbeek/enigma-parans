unit unitabout;

{$mode objfpc}{$H+}

interface

uses
  Classes, Controls, Dialogs, ExtCtrls, Forms, Graphics, StdCtrls, SysUtils, unittranslation;

type

  { TFormAbout }

  TFormAbout = class(TForm)
    BtnClose: TButton;
    Image1: TImage;
    LblAuthor: TLabel;
    LblCopyLeft: TLabel;
    LblExplanation: TLabel;
    LblImage: TLabel;
    LblSite: TLabel;
    LblTitle: TLabel;
    LblVersion: TLabel;
    procedure BtnCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    Rosetta: TRosetta;
    procedure Populate;
  public

  end;

var
  FormAbout: TFormAbout;

implementation

uses
  unitconst;

const
  SECTION = 'formabout';

{$R *.lfm}

{ TFormAbout }

procedure TFormAbout.FormCreate(Sender: TObject);
begin
  Rosetta := TRosetta.Create;
end;

procedure TFormAbout.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFormAbout.FormShow(Sender: TObject);
begin
  Populate;
end;

procedure TFormAbout.Populate;
begin
  with Rosetta do begin
    Caption := GetText(SECTION, 'formtitle');
    LblTitle.Caption := GetText(SECTION, 'formtitle');
    LblVersion.Caption := GetText(SECTION, 'version');
    LblExplanation.Caption := GetText(SECTION, 'explanation');
    LblCopyLeft.Caption := GetText(SECTION, 'copyleft');
    LblAuthor.Caption := GetText(SECTION, 'author');
    LblSite.Caption := GetText(SECTION, 'site');
    LblImage.Caption := GetText(SECTION, 'image');;
    BtnCLose.Caption := GetText(SHARED_SECTION, 'close');
  end;
end;

end.

