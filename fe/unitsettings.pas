{ Jan Kampherbeek, (c)  2021.
  Enigma Parans is open source.
  Please check the file copyright.txt in the root of the source for further details }

unit unitsettings;

{< Screen for editing the settings. }

{$mode objfpc}{$H+}

interface

uses
  Classes, Controls, Dialogs, Forms, Graphics, HelpIntfs,
  Ipfilebroker, StdCtrls, SysUtils, unitcentralcontroller, UnitDomainXchg,
  UnitLog, unittranslation;

type

  { TFormSettings }

  TFormSettings = class(TForm)
    BtnCancel: TButton;
    BtnHelp: TButton;
    BtnSave: TButton;
    CbModernPlanets: TCheckBox;
    EdOrbMinutes: TEdit;
    EdOrbSeconds: TEdit;
    LblMin: TLabel;
    LblOrbis: TLabel;
    LblSec: TLabel;
    LblTitle: TLabel;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnHelpClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure EdOrbMinutesChange(Sender: TObject);
    procedure EdOrbSecondsChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    Rosetta: TRosetta;
    CenCon: TCenCon;
    Settings: TSettings;
    Logger: TLogger;
    ModernPlanets, InputOk: boolean;
    OrbInSeconds, OrbSeconds, OrbMinutes: integer;
    procedure Populate;
    procedure DefineCurrentValues;
    procedure Validate;
  public

  end;

var
  FormSettings: TFormSettings;

implementation

{$R *.lfm}
uses
  unitconst;

const
  SECTION = 'formsettings';

{ TFormSettings }

procedure TFormSettings.FormCreate(Sender: TObject);
begin
  Rosetta := TRosetta.Create;
  CenCon := TCenCon.Create;
  Logger := TLogger.Create;
end;

procedure TFormSettings.BtnSaveClick(Sender: TObject);
begin
  ModernPlanets := CbModernPlanets.Checked;
  OrbInSeconds := OrbMinutes * 60 + OrbSeconds;
  Settings := TSettings.Create(ModernPlanets, OrbInSeconds);
  CenCon.SetSettings(Settings);
  Close;
end;

procedure TFormSettings.BtnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TFormSettings.BtnHelpClick(Sender: TObject);
var
  HelpText: string;
begin
  if Rosetta.CurrentLanguage = 'en' then HelpText := 'html/en_config.html'
  else
    HelpText := 'html/nl_config.html';
  ShowHelpOrErrorForKeyword(EmptyStr, HelpText);
end;

procedure TFormSettings.EdOrbMinutesChange(Sender: TObject);
begin
  Validate;
end;

procedure TFormSettings.EdOrbSecondsChange(Sender: TObject);
begin
  Validate;
end;

procedure TFormSettings.FormShow(Sender: TObject);
begin
  Populate;
  DefineCurrentValues;
  Validate;
end;

procedure TFormSettings.Populate;
begin
  BtnSave.Enabled := False;
  with Rosetta do begin
    Caption := GetText(SECTION, 'formtitle');
    LblTitle.Caption := GetText(SECTION, 'formtitle');
    CbModernPlanets.Caption := GetText(SECTION, 'modernplanets');
    CbModernPlanets.Hint := GetText(SECTION, 'hintmodernplanets');
    LblOrbis.Caption := GetText(SECTION, 'orbis');
    LblMin.Caption := GetText(SECTION, 'minutes');
    EdOrbMinutes.Hint := GetText(SECTION, 'hintminute');
    LblSec.Caption := GetText(SECTION, 'seconds');
    EdORbSeconds.Hint := GetText(SECTION, 'hintsecond');
    BtnSave.Caption := GetText(SHARED_SECTION, 'save');
    BtnCancel.Caption := GetText(SHARED_SECTION, 'cancel');
    BtnHelp.Caption := GetText(SHARED_SECTION, 'help');
  end;
end;

procedure TFormSettings.DefineCurrentValues;
var
  MinuteOrb, SecondOrb: integer;
begin
  CbModernPlanets.Checked := CenCon.Settings.ModernPlanets;
  MinuteOrb := CenCon.Settings.OrbSeconds div 60;
  SecondOrb := CenCon.Settings.OrbSeconds mod 60;
  EdOrbMinutes.Caption := IntToStr(MinuteOrb);
  EdOrbSeconds.Caption := IntToStr(SecondOrb);
end;

procedure TFormSettings.Validate;
var
  ErrorCode: integer;
  TotalOrb: double;
begin
  BtnSave.Enabled := False;
  InputOk := True;
  EdOrbMinutes.Color := clDefault;
  EdOrbSeconds.Color := clDefault;
  if Trim(EdOrbMinutes.Caption) = '' then OrbMinutes := 0
  else begin
    Val(EdOrbMinutes.Caption, OrbMinutes, errorCode);
    if (errorCode <> 0) then begin
      EdOrbMinutes.Color := clYellow;
      InputOk := False;
    end;
  end;
  if Trim(EdOrbSeconds.Caption) = '' then OrbSeconds := 0
  else begin
    Val(EdOrbSeconds.Caption, OrbSeconds, errorCode);
    if (errorCode <> 0) then begin
      EdOrbSeconds.Color := clYellow;
      InputOk := False;
    end;
  end;
  OrbMinutes := Abs(OrbMinutes);
  OrbSeconds := Abs(OrbSeconds);
  TotalOrb := OrbMinutes + OrbSeconds / 60.0;
  if ((TotalOrb < (1 / 60.0)) or (TotalOrb > 10.00001)) then begin
    inputOk := False;
    EdOrbMinutes.Color := clYellow;
    EdOrbSeconds.Color := clYellow;
    Logger.log('Error for input orb in TFormSettings, values (MS): ' + EdOrbMinutes.Caption +
      ', ' + EdOrbSeconds.Caption);
  end;
  if (inputOk) then BtnSave.Enabled := True;
end;




end.
