{ Jan Kampherbeek, (c)  2021.
  Enigma Parans is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit unitformatting;

{< Formatting output for UI.}

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, UnitDomainXchg;

type

  TEnumSexagType = (dms, time);         // resp. degrees, minutes, seconds and time


  { TSexagesimalPos }

  TSexagesimalPos = class
  strict private
    DecimalDegrees: double;
    SizeIntegerPart: integer;
    SexagType: TEnumSexagType;
    function DefineText: string;
  public
    { PSizeIntegerPart should be 2 or 3. If not, it defaults to 3. }
    function FormatToText(PDecimalDegrees: double; PSizeIntegerPart: integer; PSexagType: TEnumSexagType): string;
  end;

  { TDmsGlyphPos
    Not used in version 1.0 }

  TDmsGlyphPos = class
  strict private
    SexagPos: TSexagesimalPos;
    function DefineTextAndGlyph(PDegrees: double): TPositionTextWithGlyph;
  public
    constructor Create;
    function FormatToTextAndGlyph(PDecimalDegrees: double): TPositionTextWithGlyph;
  end;


implementation

uses
  UnitConst;

const
  GlyphArray: array [1..12] of string = ('1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=');  // Aries..Pisces


{ TDmsGlyphPos }

function TDmsGlyphPos.DefineTextAndGlyph(PDegrees: double): TPositionTextWithGlyph;
var
  SignIndex: integer;
  Degrees, DegreesInSign: double;
  PositionTextWithGlyph: TPositionTextWithGlyph;
begin
  Degrees := PDegrees;
  SignIndex := trunc(Degrees / 30) + 1;
  PositionTextWithGlyph.Glyph := GlyphArray[SignIndex];
  DegreesInsign := Degrees - ((SignIndex - 1) * 30);
  PositionTextWithGlyph.PositionText := SexagPos.FormatToText(DegreesInSign, 2, dms);
  Result := PositionTextWithGlyph;
end;

constructor TDmsGlyphPos.Create;
begin
  SexagPos := TSexagesimalPos.Create;
end;

function TDmsGlyphPos.FormatToTextAndGlyph(PDecimalDegrees: double): TPositionTextWithGlyph;
begin
  Result := DefineTextAndGlyph(PDecimalDegrees);
end;

{ TSexagesimalPos }

function TSexagesimalPos.FormatToText(PDecimalDegrees: double; PSizeIntegerPart: integer;
  PSexagType: TEnumSexagType): string;
begin
  DecimalDegrees := PDecimalDegrees;
  if (2 > PSizeIntegerPart) or (3 < PSizeIntegerPart) then SizeIntegerPart := 3
  else
    SizeIntegerPart := PSizeIntegerPart;
  SexagType := PSexagType;
  Result := DefineText;
end;

function TSexagesimalPos.DefineText: string;
var
  Degrees, Minutes, Seconds: integer;
  TempAmount: double;
  FmtDH, FmtMs, Separator1, Separator2, Separator3, FormattedText: string;
  Negative: boolean;
begin
  Negative := (0 > DecimalDegrees);
  if (SexagType = dms) then begin
    Separator1 := DEGREE_SIGN;
    Separator2 := MINUTE_SIGN;
    Separator3 := SECOND_SIGN;
  end else begin
    Separator1 := TIME_SEPARATOR;
    Separator2 := TIME_SEPARATOR;
    Separator3 := EMPTY_STRING;
  end;
  Degrees := Trunc(Abs(DecimalDegrees));
  TempAmount := Frac(Abs(DecimalDegrees)) * MINUTES_IN_DEGREE_HOUR;
  Minutes := Trunc(TempAmount);
  Seconds := Trunc(frac(TempAmount) * SECONDS_IN_MINUTE);
  if (SizeIntegerPart = 2) then FmtDh := '%2.2d'
  else
    FmtDh := '%3.3d';
  FmtMs := '%2.2d';
  FormattedText := Format(FmtDh, [Degrees]) + Separator1 + Format(FmtMs, [Minutes]) +
    Separator2 + Format(FmtMs, [Seconds]) + Separator3;
  if (Negative) then FormattedText := '-' + FormattedText;
  Result := FormattedText;
end;


end.
