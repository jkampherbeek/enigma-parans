{ Jan Kampherbeek, (c)  2021.
  Enigma Parans is open source.
  Please check the file copyright.txt in the root of the source for further details }
unit unitconst;

{< Global constants. }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

const
  SPACE = ' ';
  EMPTY_STRING = '';
  TIME_SEPARATOR = ':';
  DEGREE_SIGN = '°';
  MINUTE_SIGN = '''';
  SECOND_SIGN = '"';
  ZERO_DOUBLE = 0.0;
  ZERO_INT = 0;
  MINUTES_IN_HOUR = 60;
  MINUTES_IN_DEGREE_HOUR = 60.0;
  SECONDS_IN_HOUR = 3600;
  SECONDS_IN_MINUTE = 60.0;
  MIN_MINUTE_SECOND = 0;
  MIN_MINUTE_ORB_IN_TIME = 0;
  MAX_MINUTE_SECOND = 59;
  MAX_MINUTE_ORB_IN_TIME = 15;
  LOOKUP_SECTION = 'lookup';
  SHARED_SECTION = 'shared';
  MENU_SECTION = 'menu';
  MIN_LONGITUDE_DEGREES = -180;
  MAX_LONGITUDE_DEGREES = 180;
  MIN_LATITUDE_DEGREES = -89;
  MAX_LATITUDE_DEGREES = 89;
  LATITUDE_NORTH = 'N';
  LATITUDE_SOUTH = 'S';
  LONGITUDE_EAST = 'E';
  LONGITUDE_WEST = 'W';
  MIN_HOUR = 0;
  MAX_HOUR = 23;
  MIN_MINUTE = 0;
  MAX_Minute = 59;
  MIN_SECOND = 0;
  MAX_SECOND = 59;
  MIN_JD_ASTRON = -3026613.5;
  MAX_JD = 7857131.5;


implementation

end.

